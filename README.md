### Một số thứ cần cho project này ###

* Visual studio 2015 update 3 trở lên
* .NET Core, có thể download tại đây: https://www.microsoft.com/net/download/core. Down sdk và runtime
* Chạy gulp restore để restore các file/folder cần thiết

### Chạy code này nếu bị lỗi jquery ###

[Link tham khảo](https://stackoverflow.com/questions/30623825/how-to-use-jquery-with-angular2)

```
tsd install jquery --save
or
typings install dt~jquery --global --save
```

### Account của production ###

```
ID: admin@rrbank.com
Password: RRBank@123Com
```