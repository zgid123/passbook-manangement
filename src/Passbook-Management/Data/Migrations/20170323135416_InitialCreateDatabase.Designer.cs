﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Passbook_Management.Data;

namespace PassbookManagement.Data.Migrations
{
    [DbContext(typeof(PassbookManagementContext))]
    [Migration("20170323135416_InitialCreateDatabase")]
    partial class InitialCreateDatabase
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ChangeDetector.SkipDetectChanges", "true")
                .HasAnnotation("ProductVersion", "1.1.1");
        }
    }
}
