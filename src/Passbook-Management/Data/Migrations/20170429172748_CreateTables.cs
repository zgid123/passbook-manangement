﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PassbookManagement.Data.Migrations
{
    public partial class CreateTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BankAccounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false),
                    Number = table.Column<string>(maxLength: 12, nullable: false),
                    Balance = table.Column<long>(),
                    TypeAccount = table.Column<int>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BankAccounts_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "SavingAccountsType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 256),
                    InterestRate = table.Column<double>(),
                    Term = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavingAccountsType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SavingAccounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(),
                    ClosedAt = table.Column<DateTime>(nullable: true),
                    InterestRate = table.Column<double>(),
                    Term = table.Column<double>(nullable: true),
                    Active = table.Column<bool>(defaultValue: true),
                    Reason = table.Column<string>(maxLength: 256),
                    Status = table.Column<char>(),
                    BankAccountId = table.Column<int>(nullable: false),
                    SavingAccountTypeId = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavingAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SavingAccounts_BankAccounts_BankAccountId",
                        column: x => x.BankAccountId,
                        principalTable: "BankAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_SavingAccounts_SavingAccountsType_SavingAccountTypeId",
                        column: x => x.SavingAccountTypeId,
                        principalTable: "SavingAccountsType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_SavingAccounts_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "TransactionHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    RequestedAt = table.Column<DateTime>(),
                    TransactionType = table.Column<char>(),
                    SavingAccountId = table.Column<int>(nullable: false),
                    PersonName = table.Column<string>(maxLength: 50),
                    EmployeeId = table.Column<int>(),
                    Amount = table.Column<long>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransactionHistories_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_TransactionHistories_SavingAccounts_SavingAccountId",
                        column: x => x.SavingAccountId,
                        principalTable: "SavingAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateIndex(
                name: "BankAccountsNumberIndex",
                table: "BankAccounts",
                column: "Number",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "SavingAccountsTypeNameIndex",
                table: "SavingAccountsType",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "SavingAccountsIdIndex",
                table: "SavingAccounts",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "TransactionHistoriesRequestedAtIndex",
                table: "TransactionHistories",
                column: "RequestedAt",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankAccounts");

            migrationBuilder.DropTable(
                name: "SavingAccountsType");

            migrationBuilder.DropTable(
                name: "SavingAccounts");

            migrationBuilder.DropTable(
                name: "TransactionHistories");
        }
    }
}
