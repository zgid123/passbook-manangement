﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Data
{
    public class PassbookManagementContextFactory : IDbContextFactory<PassbookManagementContext>
    {
        public PassbookManagementContext Create(DbContextFactoryOptions options)
        {
            var optionsBuilder = new DbContextOptionsBuilder<PassbookManagementContext>();
            optionsBuilder.UseSqlite("Data Source=PassbookManagement.db");

            return new PassbookManagementContext(optionsBuilder.Options);
        }
    }
}
