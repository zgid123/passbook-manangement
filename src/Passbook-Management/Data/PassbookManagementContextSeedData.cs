﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Passbook_Management.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Data
{
    public class PassbookManagementContextSeedData
    {
        public static async void Seed(IApplicationBuilder applicationBuilder)
        {
            using (var context = applicationBuilder.ApplicationServices.GetRequiredService<PassbookManagementContext>())
            {
                //context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                if (!context.Roles.Any(r => r.Name == "Admin"))
                {
                    context.Roles.Add(new IdentityRole { Name = "Admin", NormalizedName = "Admin" });
                }
                if (!context.Roles.Any(r => r.Name == "Employee"))
                {
                    context.Roles.Add(new IdentityRole { Name = "Employee", NormalizedName = "Employee" });
                }
                if (!context.Roles.Any(r => r.Name == "Manager"))
                {
                    context.Roles.Add(new IdentityRole { Name = "Manager", NormalizedName = "Manager" });
                }
                if (!context.Roles.Any(r => r.Name == "Customer"))
                {
                    context.Roles.Add(new IdentityRole { Name = "Customer", NormalizedName = "Customer" });
                }

                context.SaveChanges();

                if (!context.Users.Any(u => u.Email == "admin@rrbank.com"))
                {
                    using (var userManager = applicationBuilder.ApplicationServices.GetService<UserManager<User>>())
                    {
                        var user = new User
                        {
                            Email = "admin@rrbank.com",
                            UserName = "admin@rrbank.com",
                            NormalizedUserName = "Admin",
                            Name = "Admin",
                            CreatedAt = DateTime.Now
                        };

                        var result = await userManager.CreateAsync(user, "RRBank@123Com");

                        if (result.Succeeded)
                        {
                            result = await userManager.AddToRoleAsync(user, "Admin");

                            context.Employees.Add(new Employee { UserId = user.Id });
                        }
                    }
                }

                if (!context.SavingAccountTypes.Any(sat => sat.Name == "Không kỳ hạn"))
                {
                    context.SavingAccountTypes.Add(new SavingAccountType { Name = "Không kỳ hạn", InterestRate = 0.2 });
                }

                if (!context.SavingAccountTypes.Any(sat => sat.Name == "3 tháng"))
                {
                    context.SavingAccountTypes.Add(new SavingAccountType { Name = "3 tháng", InterestRate = 0.3, Term = 0.5 });
                }

                if (!context.SavingAccountTypes.Any(sat => sat.Name == "Không kỳ hạn"))
                {
                    context.SavingAccountTypes.Add(new SavingAccountType { Name = "6 tháng", InterestRate = 0.5, Term = 0.55 });
                }

                if (!context.AdminSettings.Any(ads => ads.Name == "Tiền gửi tối thiểu"))
                {
                    context.AdminSettings.Add(new AdminSetting { Name = "Tiền gửi tối thiểu", Value = 100000 });
                }

                if (!context.AdminSettings.Any(ads => ads.Name == "Tiền tạo tài khoản tối thiểu"))
                {
                    context.AdminSettings.Add(new AdminSetting { Name = "Tiền tạo tài khoản tối thiểu", Value = 100000 });
                }

                if (!context.AdminSettings.Any(ads => ads.Name == "Số ngày tối thiểu được phép rút tiền"))
                {
                    context.AdminSettings.Add(new AdminSetting { Name = "Số ngày tối thiểu được phép rút tiền", Value = 15 });
                }

                if (!context.AdminSettings.Any(ads => ads.Name == "Số tháng tối thiểu được tính lãi suất"))
                {
                    context.AdminSettings.Add(new AdminSetting { Name = "Số tháng tối thiểu được tính lãi suất", Value = 1 });
                }

                context.SaveChanges();
            }
        }
    }
}
