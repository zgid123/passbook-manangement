﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Passbook_Management.Data;
using Passbook_Management.Models.View_Models;
using Passbook_Management.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Passbook_Management.Extensions;
using NodaTime;

namespace Passbook_Management.Services
{
    public class SavingAccountService : Service
    {
        private readonly UserAccountService _UserAccountService;

        public SavingAccountService(IMapper mapper, PassbookManagementContext context, SignInManager<User> signInManager, UserManager<User> userManager)
            : base(mapper, context)
        {
            _UserAccountService = new UserAccountService(signInManager, userManager, mapper);
        }

        public async Task<SavingAccountViewModel> CreateSavingAccount(SavingAccountViewModel savingAccount)
        {
            User user = _Context.Users.SingleOrDefault(u => u.PersonalIdentity == savingAccount.BankAccount.User.PersonalIdentity);
            BankAccount bankAccount = null;
            try
            {
                if (user == null)
                {
                    var result = await _UserAccountService.CreateUserAccount(savingAccount.BankAccount.User);

                    if (result.Succeeded)
                    {
                        user = await _UserAccountService.GetUserByEmail(savingAccount.BankAccount.User.Email);
                        result = await _UserAccountService.AddToRole(user, "Customer");
                    }
                }

                if (user == null)
                {
                    if (user != null)
                    {
                        var result = await _UserAccountService.DeleteUser(user);
                    }
                    return null;
                }
                else
                {
                    bankAccount = new BankAccount
                    {
                        AccountType = (int)BankAccountViewModel.TypeAccountNames.SavingAccount,
                        Balance = savingAccount.BankAccount.Balance,
                        Number = GenerateRandomString.StringNumber(12),
                    };

                    if (user.BankAccounts == null)
                    {
                        user.BankAccounts = new List<BankAccount>();
                    }

                    user.BankAccounts.Add(bankAccount);
                }

                var createdAt = savingAccount.CreatedAt;
                DateTime? closedAt;
                Employee employee = _Context.Employees.Single(e => e.UserId == savingAccount.Employee.User.Uuid);

                if (savingAccount.SavingAccountType.Name == "Không kỳ hạn")
                {
                    closedAt = null;
                }
                else
                {
                    closedAt = createdAt.AddMonths(int.Parse(savingAccount.SavingAccountType.Name.Replace("tháng", "").Trim()));
                }

                bankAccount.SavingAccount = new SavingAccount
                {
                    Active = true,
                    CreatedAt = savingAccount.CreatedAt,
                    ClosedAt = closedAt,
                    Employee = employee,
                    InterestRate = savingAccount.SavingAccountType.InterestRate,
                    Term = savingAccount.SavingAccountType.Term,
                    BankAccount = bankAccount,
                    SavingAccountTypeId = savingAccount.SavingAccountType.Id
                };

                if (bankAccount.SavingAccount.TransactionHistories == null)
                {
                    bankAccount.SavingAccount.TransactionHistories = new List<TransactionHistory>();
                }

                bankAccount.SavingAccount.TransactionHistories.Add(new TransactionHistory
                {
                    TransactionType = (int)TransactionHistoryViewModel.Types.Creation,
                    Amount = savingAccount.BankAccount.Balance,
                    Employee = employee,
                    RequestedAt = DateTime.Now,
                    PersonName = savingAccount.BankAccount.User.Name
                });

                _Context.SaveChanges();

                return _Mapper.Map<SavingAccount, SavingAccountViewModel>(bankAccount.SavingAccount);
            }
            catch
            {
                if (user != null)
                {
                    _Context.TransactionHistories.RemoveRange(bankAccount.SavingAccount.TransactionHistories);
                    _Context.SavingAccounts.Remove(bankAccount.SavingAccount);
                    _Context.BankAccounts.Remove(bankAccount);
                    _Context.SaveChanges();

                    var result = await _UserAccountService.DeleteUser(user);
                }
                return null;
            }
        }

        public SavingAccountViewModel UpdateSavingAccount(SavingAccountViewModel savingAccount)
        {
            SavingAccount currentSavingAccount = _Context.SavingAccounts
                                                  .Include(sa => sa.BankAccount).ThenInclude(ba => ba.User)
                                                  .Include(sa => sa.SavingAccountType)
                                                  .Include(sa => sa.TransactionHistories)
                                                  .SingleOrDefault(sa => sa.Id == savingAccount.Id);

            if (currentSavingAccount == null)
            {
                return null;
            }

            try
            {
                currentSavingAccount.CreatedAt = savingAccount.CreatedAt;
                if (currentSavingAccount.SavingAccountTypeId != savingAccount.SavingAccountType.Id)
                {
                    currentSavingAccount.SavingAccountTypeId = savingAccount.SavingAccountType.Id;
                    currentSavingAccount.InterestRate = savingAccount.SavingAccountType.InterestRate;
                    currentSavingAccount.Term = savingAccount.SavingAccountType.Term;
                }

                if (savingAccount.SavingAccountType.Name == "Không kỳ hạn")
                {
                    currentSavingAccount.ClosedAt = null;
                }
                else
                {
                    currentSavingAccount.ClosedAt = currentSavingAccount.CreatedAt.AddMonths(int.Parse(savingAccount.SavingAccountType.Name.Replace("tháng", "").Trim()));
                }

                currentSavingAccount.BankAccount.Balance = savingAccount.BankAccount.Balance;

                User user = _Context.Users.SingleOrDefault(u => u.PersonalIdentity == savingAccount.BankAccount.User.PersonalIdentity);
                Employee employee = _Context.Employees.Single(e => e.UserId == savingAccount.Employee.User.Uuid);

                if (user != null)
                {
                    currentSavingAccount.BankAccount.User = user;
                }
                else
                {
                    currentSavingAccount.BankAccount.User.Name = savingAccount.BankAccount.User.Name;
                    currentSavingAccount.BankAccount.User.PersonalIdentity = savingAccount.BankAccount.User.PersonalIdentity;
                    currentSavingAccount.BankAccount.User.PhoneNumber = savingAccount.BankAccount.User.PhoneNumber;
                    currentSavingAccount.BankAccount.User.Address = savingAccount.BankAccount.User.Address;
                    currentSavingAccount.BankAccount.User.DateOfBirth = savingAccount.BankAccount.User.DateOfBirth;
                    currentSavingAccount.BankAccount.User.Email = savingAccount.BankAccount.User.Email;
                }

                currentSavingAccount.TransactionHistories.Add(new TransactionHistory
                {
                    TransactionType = (int)TransactionHistoryViewModel.Types.Update,
                    Amount = savingAccount.BankAccount.Balance,
                    Employee = employee,
                    RequestedAt = DateTime.Now,
                    PersonName = savingAccount.BankAccount.User.Name
                });

                _Context.SaveChanges();

                return _Mapper.Map<SavingAccount, SavingAccountViewModel>(currentSavingAccount);
            }
            catch
            {
                return null;
            }
        }

        public SavingAccountViewModel GetById(int id)
        {
            var savingAccount = _Mapper.Map<SavingAccount, SavingAccountViewModel>(
                                    _Context.SavingAccounts
                                    .Include(sa => sa.BankAccount).ThenInclude(ba => ba.User)
                                    .Include(sa => sa.SavingAccountType)
                                    .Include(sa => sa.Employee).ThenInclude(e => e.User)
                                    .SingleOrDefault(sa => sa.Id == id)
                                );
            AdminSetting minimumMonthRate = _Context.AdminSettings.SingleOrDefault(ads => ads.Name == AdminSettingViewModel.SettingNames.MinimumMonthRate.ToValue());
            var now = DateTime.Now;
            LocalDateTime end = new LocalDateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute);

            LocalDateTime start = new LocalDateTime(savingAccount.CreatedAt.Year, savingAccount.CreatedAt.Month, savingAccount.CreatedAt.Day, savingAccount.CreatedAt.Hour, savingAccount.CreatedAt.Minute);

            Period period = Period.Between(start, end);

            long balance = 0;

            if (savingAccount.SavingAccountType.Name == "Không kỳ hạn")
            {
                int month = period.Months;
                long temp = savingAccount.BankAccount.Balance;

                if (month > (int)minimumMonthRate.Value)
                {
                    for (int i = 0, n = month; i < n; i++)
                    {
                        balance += (long)(temp * savingAccount.InterestRate);
                        temp = balance;
                    }
                }
            }
            else
            {
                int monthTerm = int.Parse(savingAccount.SavingAccountType.Name.Replace(" tháng", "").Trim());
                int term = period.Months / monthTerm;

                balance = (long)(savingAccount.BankAccount.Balance * savingAccount.InterestRate * savingAccount.Term.Value * term);
            }

            savingAccount.Interest = balance;

            return savingAccount;
        }

        public List<SavingAccountViewModel> GetAll(int page, SavingAccountSearchViewModel search)
        {
            var result = _Context.SavingAccounts
                                 .Include(sa => sa.SavingAccountType)
                                 .Include(sa => sa.BankAccount).ThenInclude(ba => ba.User)
                                 .Where(sa => sa.BankAccount.Number.Contains(search.BankAccountNumber)
                                    && sa.SavingAccountType.Name.Contains(search.SavingAccountType)
                                    && sa.BankAccount.User.Name.Contains(search.UserName)
                                    && sa.BankAccount.Balance >= search.FromBalance);
            if (search.ToBalance.HasValue)
            {
                result = result.Where(sa => sa.BankAccount.Balance <= search.ToBalance);
            }

            if (search.StartDate.HasValue)
            {
                result = result.Where(sa => sa.CreatedAt.Date >= search.StartDate.Value.ToLocalTime().Date);
            }

            if (search.EndDate.HasValue)
            {
                result = result.Where(sa => sa.CreatedAt.Date <= search.EndDate.Value.ToLocalTime().Date);
            }

            return _Mapper.Map<List<SavingAccount>, List<SavingAccountViewModel>>(result.OrderByDescending(sa => sa.CreatedAt).Skip((page - 1) * 12).Take(12).ToList());
        }

        public SavingAccountViewModel Deposit(DepositViewModel depositViewModel)
        {
            SavingAccount currentSavingAccount = _Context.SavingAccounts
                                                         .Include(sa => sa.BankAccount)
                                                         .Include(sa => sa.SavingAccountType)
                                                         .Include(sa => sa.TransactionHistories)
                                                         .SingleOrDefault(sa => sa.Id == depositViewModel.SavingAccount.Id);
            AdminSetting minimumDeposit = _Context.AdminSettings.SingleOrDefault(ads => ads.Name == AdminSettingViewModel.SettingNames.MinimumDeposit.ToValue());

            if (currentSavingAccount == null)
            {
                return null;
            }

            if (depositViewModel.Value < minimumDeposit.Value)
            {
                depositViewModel.SavingAccount.Id = -1;
                depositViewModel.SavingAccount.InterestRate = minimumDeposit.Value;
                return depositViewModel.SavingAccount;
            }

            if (depositViewModel.Value > 3000000000)
            {
                depositViewModel.SavingAccount.Id = -2;
                return depositViewModel.SavingAccount;
            }

            if (currentSavingAccount.SavingAccountType.Name != "Không kỳ hạn")
            {
                depositViewModel.SavingAccount.Id = -3;
                return depositViewModel.SavingAccount;
            }

            Employee employee = _Context.Employees.SingleOrDefault(e => e.UserId == depositViewModel.SavingAccount.Employee.User.Uuid);

            if (employee == null)
            {
                return null;
            }

            try
            {
                currentSavingAccount.BankAccount.Balance += depositViewModel.Value;

                currentSavingAccount.TransactionHistories.Add(new TransactionHistory
                {
                    Amount = depositViewModel.Value,
                    Employee = employee,
                    PersonName = depositViewModel.Trader,
                    RequestedAt = DateTime.Now,
                    TransactionType = (int)TransactionHistoryViewModel.Types.Deposit
                });

                _Context.SaveChanges();

                return _Mapper.Map<SavingAccount, SavingAccountViewModel>(currentSavingAccount);
            }
            catch
            {
                return null;
            }
        }

        public SavingAccountViewModel Withdraw(WithdrawViewModel withdrawViewModel)
        {
            SavingAccount currentSavingAccount = _Context.SavingAccounts
                                                         .Include(sa => sa.BankAccount)
                                                         .Include(sa => sa.SavingAccountType)
                                                         .Include(sa => sa.TransactionHistories)
                                                         .SingleOrDefault(sa => sa.Id == withdrawViewModel.SavingAccount.Id);

            if (currentSavingAccount == null)
            {
                return null;
            }

            if (withdrawViewModel.Value < 0)
            {
                withdrawViewModel.SavingAccount.Id = -1;
                return withdrawViewModel.SavingAccount;
            }

            Employee employee = _Context.Employees.SingleOrDefault(e => e.UserId == withdrawViewModel.SavingAccount.Employee.User.Uuid);

            if (employee == null)
            {
                return null;
            }

            try
            {
                LocalDateTime start = new LocalDateTime(currentSavingAccount.CreatedAt.Year, currentSavingAccount.CreatedAt.Month, currentSavingAccount.CreatedAt.Day, currentSavingAccount.CreatedAt.Hour, currentSavingAccount.CreatedAt.Minute);
                var now = DateTime.Now;
                LocalDateTime end = new LocalDateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute);

                Period period = Period.Between(start, end);
                int months = period.Months;
                long balance;

                if (currentSavingAccount.SavingAccountType.Name == "Không kỳ hạn")
                {
                    AdminSetting minimumDayWithdraw = _Context.AdminSettings.SingleOrDefault(ads => ads.Name == AdminSettingViewModel.SettingNames.MinimumDayWithdraw.ToValue());
                    AdminSetting minimumMonthRate = _Context.AdminSettings.SingleOrDefault(ads => ads.Name == AdminSettingViewModel.SettingNames.MinimumMonthRate.ToValue());

                    if (period.Days < minimumDayWithdraw.Value)
                    {
                        withdrawViewModel.SavingAccount.Id = -2;
                        withdrawViewModel.SavingAccount.Status = (int)minimumDayWithdraw.Value;
                        return withdrawViewModel.SavingAccount;
                    }

                    balance = currentSavingAccount.BankAccount.Balance;

                    if (months > (int)minimumMonthRate.Value)
                    {
                        for (int i = 0, n = months; i < n; i++)
                        {
                            balance += (long)(balance * currentSavingAccount.InterestRate);
                        }
                    }

                    if (withdrawViewModel.Value >= balance)
                    {
                        currentSavingAccount.BankAccount.Balance = 0;
                        withdrawViewModel.SavingAccount.BankAccount.Balance = balance;
                    }
                    else
                    {
                        currentSavingAccount.BankAccount.Balance = balance - withdrawViewModel.Value;
                        withdrawViewModel.SavingAccount.BankAccount.Balance = withdrawViewModel.Value;
                    }
                }
                else
                {
                    int monthTerm = int.Parse(currentSavingAccount.SavingAccountType.Name.Replace(" tháng", "").Trim());
                    int term = months / monthTerm;

                    if (term == 0)
                    {
                        withdrawViewModel.SavingAccount.Id = -3;
                        withdrawViewModel.SavingAccount.Status = monthTerm;
                        return withdrawViewModel.SavingAccount;
                    }

                    balance = (long)(currentSavingAccount.BankAccount.Balance + currentSavingAccount.BankAccount.Balance * currentSavingAccount.InterestRate * currentSavingAccount.Term.Value * term);
                    currentSavingAccount.BankAccount.Balance = 0;
                    withdrawViewModel.SavingAccount.BankAccount.Balance = balance;
                }

                currentSavingAccount.TransactionHistories.Add(new TransactionHistory
                {
                    Amount = withdrawViewModel.Value,
                    Employee = employee,
                    PersonName = withdrawViewModel.Trader,
                    RequestedAt = DateTime.Now,
                    TransactionType = (int)TransactionHistoryViewModel.Types.Withdraw
                });

                if (currentSavingAccount.BankAccount.Balance == 0)
                {
                    currentSavingAccount.TransactionHistories.Add(new TransactionHistory
                    {
                        Employee = employee,
                        PersonName = "System",
                        RequestedAt = DateTime.Now,
                        TransactionType = (int)TransactionHistoryViewModel.Types.Closure
                    });

                    currentSavingAccount.Active = false;
                }

                _Context.SaveChanges();

                return withdrawViewModel.SavingAccount;
            }
            catch
            {
                return null;
            }
        }
    }
}
