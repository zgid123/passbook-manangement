﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Passbook_Management.Data;
using Passbook_Management.Models.View_Models;
using Microsoft.EntityFrameworkCore;

namespace Passbook_Management.Services
{
    public class ReportService : Service
    {
        public ReportService(IMapper mapper, PassbookManagementContext context) : base(mapper, context) { }

        public List<DailyIncomeReportViewModel> DailyIncomeReport(DateTime date)
        {
            return _Context.SavingAccountTypes.GroupJoin(_Context.TransactionHistories
                                                                 .Where(th => th.RequestedAt.Date == date.ToLocalTime().Date)
                                                                 .Include(th => th.SavingAccount).ThenInclude(sa => sa.SavingAccountType),
                                                         sat => sat.Name,
                                                         th => th.SavingAccount.SavingAccountType.Name,
                                                         (sat, th) => new
                                                         {
                                                             Name = sat.Name,
                                                             Transactions = th
                                                         })
                                              .Select(satth => new DailyIncomeReportViewModel
                                              {
                                                  SavingAccountType = satth.Name,
                                                  Income = satth.Transactions.Where(th => th.TransactionType == (int)TransactionHistoryViewModel.Types.Deposit
                                                                                      || th.TransactionType == (int)TransactionHistoryViewModel.Types.Creation)
                                                                             .Sum(th => th.Amount),
                                                  Disbursement = satth.Transactions.Where(th => th.TransactionType == (int)TransactionHistoryViewModel.Types.Withdraw)
                                                                                   .Sum(th => th.Amount)
                                              }).OrderByDescending(dirvm => dirvm.SavingAccountType).ToList();
        }

        public List<MonthlySavingAccountsReportViewModel> MonthlySavingAccountsReport(DateTime date, string savingAccountType)
        {
            date = date.ToLocalTime();
            return _Context.TransactionHistories.Where(th => th.RequestedAt.Year == date.Year && th.RequestedAt.Month == date.Month
                                                        && th.SavingAccount.SavingAccountType.Name == savingAccountType)
                                                .GroupBy(th => th.RequestedAt)
                                                .Select(th => new MonthlySavingAccountsReportViewModel
                                                {
                                                    Date = th.Key,
                                                    Creations = th.Count(t => t.TransactionType == (int)TransactionHistoryViewModel.Types.Creation),
                                                    Closures = th.Count(t => t.TransactionType == (int)TransactionHistoryViewModel.Types.Closure)
                                                }).OrderBy(th => th.Date).ToList();
        }
    }
}
