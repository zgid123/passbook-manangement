﻿using AutoMapper;
using Passbook_Management.Data;
using Passbook_Management.Models;
using Passbook_Management.Models.View_Models;
using System.Collections.Generic;
using System.Linq;

namespace Passbook_Management.Services
{
    public class SavingAccountTypeService : Service
    {
        public SavingAccountTypeService(IMapper mapper, PassbookManagementContext context) : base(mapper, context) { }

        public IList<SavingAccountTypeViewModel> GetSavingAccountTypes()
        {
            return _Mapper.Map<List<SavingAccountType>, List<SavingAccountTypeViewModel>>(_Context.SavingAccountTypes.ToList());
        }

        public SavingAccountTypeViewModel GetById(int id)
        {
            return _Mapper.Map<SavingAccountType, SavingAccountTypeViewModel>(_Context.SavingAccountTypes.SingleOrDefault(sat => sat.Id == id));
        }

        public SavingAccountTypeViewModel Update(SavingAccountTypeViewModel savingAccountType)
        {
            SavingAccountType currentSavingAccountType = _Context.SavingAccountTypes.SingleOrDefault(sat => sat.Id == savingAccountType.Id);

            if (currentSavingAccountType == null)
            {
                return null;
            }

            if (currentSavingAccountType.Name == "Không kỳ hạn" && currentSavingAccountType.Name != savingAccountType.Name)
            {
                currentSavingAccountType.InterestRate = -1;
                return _Mapper.Map<SavingAccountType, SavingAccountTypeViewModel>(currentSavingAccountType);
            }

            int temp;
            if (!int.TryParse(savingAccountType.Name, out temp))
            {
                return null;
            }

            if (savingAccountType.InterestRate < 0)
            {
                currentSavingAccountType.InterestRate = -2;
                return _Mapper.Map<SavingAccountType, SavingAccountTypeViewModel>(currentSavingAccountType);
            }

            if (savingAccountType.Term < 0)
            {
                currentSavingAccountType.InterestRate = -3;
                return _Mapper.Map<SavingAccountType, SavingAccountTypeViewModel>(currentSavingAccountType);
            }

            try
            {
                currentSavingAccountType.Name = savingAccountType.Name.Trim() + " tháng";
                currentSavingAccountType.Term = savingAccountType.Term;
                currentSavingAccountType.InterestRate = savingAccountType.InterestRate;

                _Context.SaveChanges();

                return _Mapper.Map<SavingAccountType, SavingAccountTypeViewModel>(currentSavingAccountType);
            }
            catch
            {
                return null;
            }
        }

        public SavingAccountTypeViewModel Create(SavingAccountTypeViewModel savingAccountType)
        {
            if (savingAccountType.Name.Trim() == "Không kỳ hạn")
            {
                savingAccountType.InterestRate = -1;
                return savingAccountType;
            }

            int temp;
            if (!int.TryParse(savingAccountType.Name, out temp))
            {
                return null;
            }

            if (savingAccountType.InterestRate < 0)
            {
                savingAccountType.InterestRate = -2;
                return savingAccountType;
            }

            if (savingAccountType.Term < 0)
            {
                savingAccountType.InterestRate = -3;
                return savingAccountType;
            }

            try
            {
                _Context.SavingAccountTypes.Add(new SavingAccountType
                {
                    Name = savingAccountType.Name.Trim() + " tháng",
                    Term = savingAccountType.Term,
                    InterestRate = savingAccountType.InterestRate
                });

                _Context.SaveChanges();

                return savingAccountType;
            }
            catch
            {
                return null;
            }
        }
    }
}
