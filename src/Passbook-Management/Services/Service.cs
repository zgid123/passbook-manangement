﻿using AutoMapper;
using Passbook_Management.Data;

namespace Passbook_Management.Services
{
    public abstract class Service
    {
        protected IMapper _Mapper;
        protected PassbookManagementContext _Context;

        public Service(IMapper mapper, PassbookManagementContext context)
        {
            _Mapper = mapper;
            _Context = context;
        }
    }
}
