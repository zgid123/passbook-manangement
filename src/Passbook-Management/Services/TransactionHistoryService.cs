﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Passbook_Management.Data;
using Passbook_Management.Models.View_Models;
using Passbook_Management.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Passbook_Management.Services
{
    public class TransactionHistoryService : Service
    {
        public TransactionHistoryService(IMapper mapper, PassbookManagementContext context) : base(mapper, context) { }

        public IList<TransactionHistoryViewModel> GetTransactionHistories(int page, TransactionHistorySearch search)
        {
            var result = _Context.TransactionHistories
                                 .Include(th => th.SavingAccount).ThenInclude(sa => sa.BankAccount)
                                 .Include(th => th.Employee).ThenInclude(e => e.User)
                                 .Where(th => th.PersonName.Contains(search.Trader));

            if (search.StartDate.HasValue)
            {
                result = result.Where(th => th.RequestedAt.Date >= search.StartDate.Value.ToLocalTime().Date);
            }

            if (search.EndDate.HasValue)
            {
                result = result.Where(th => th.RequestedAt.Date <= search.EndDate.Value.ToLocalTime().Date);
            }

            List<TransactionHistory> histories = result.OrderByDescending(th => th.RequestedAt)
                                                       .Skip((page - 1) * 12).Take(12)
                                                       .ToList();
            return _Mapper.Map<List<TransactionHistory>, List<TransactionHistoryViewModel>>(histories);
        }

        public IList<TransactionHistoryViewModel> GetBySavingAccount(int id, int page, TransactionHistorySearch search)
        {
            var result = _Context.TransactionHistories.Where(th => th.SavingAccountId == id)
                                                      .Include(th => th.SavingAccount).ThenInclude(sa => sa.BankAccount)
                                                      .Include(th => th.Employee).ThenInclude(e => e.User)
                                                      .Where(th => th.PersonName.Contains(search.Trader));

            if (search.StartDate.HasValue)
            {
                result = result.Where(th => th.RequestedAt.Date >= search.StartDate.Value.ToLocalTime().Date);
            }

            if (search.EndDate.HasValue)
            {
                result = result.Where(th => th.RequestedAt.Date <= search.EndDate.Value.ToLocalTime().Date);
            }

            List<TransactionHistory> histories = result.OrderBy(th => th.RequestedAt)
                                                       .Skip((page - 1) * 12).Take(12)
                                                       .ToList();
            return _Mapper.Map<List<TransactionHistory>, List<TransactionHistoryViewModel>>(histories);
        }
    }
}
