﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Passbook_Management.Models;
using Passbook_Management.Models.View_Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Passbook_Management.Services
{
    public class UserAccountService
    {
        private readonly SignInManager<User> _SignInManager;
        private readonly UserManager<User> _UserManager;
        private readonly IMapper _Mapper;

        public UserAccountService(SignInManager<User> signInManager, UserManager<User> userManager, IMapper mapper)
        {
            _SignInManager = signInManager;
            _UserManager = userManager;
            _Mapper = mapper;
        }

        public Task<SignInResult> Login(LoginViewModel model)
        {
            return _SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
        }

        public async Task<IdentityResult> CreateUserAccount(UserViewModel user)
        {
            var newUser = _Mapper.Map<UserViewModel, User>(user);
            newUser.UserName = user.Email;
            return await _UserManager.CreateAsync(newUser, user.PersonalIdentity);
        }

        public Task<User> GetUserByEmail(string email)
        {
            return _UserManager.FindByEmailAsync(email);
        }

        public Task<User> GetUserById(string id)
        {
            return _UserManager.FindByIdAsync(id);
        }

        public Task<IList<string>> GetRoles(User user)
        {
            return _UserManager.GetRolesAsync(user);
        }

        public Task<IdentityResult> AddToRole(User user, string role)
        {
            return _UserManager.AddToRoleAsync(user, role);
        }

        public Task<IdentityResult> DeleteUser(User user)
        {
            return _UserManager.DeleteAsync(user);
        }
    }
}
