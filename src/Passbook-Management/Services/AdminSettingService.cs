﻿using System.Linq;
using AutoMapper;
using Passbook_Management.Data;
using Passbook_Management.Models;
using Passbook_Management.Models.View_Models;
using System.Collections.Generic;

namespace Passbook_Management.Services
{
    public class AdminSettingService : Service
    {
        public AdminSettingService(IMapper mapper, PassbookManagementContext context) : base(mapper, context) { }

        public List<AdminSettingViewModel> GetAll()
        {
            return _Mapper.Map<List<AdminSetting>, List<AdminSettingViewModel>>(_Context.AdminSettings.ToList());
        }

        public AdminSettingViewModel GetById(int id)
        {
            return _Mapper.Map<AdminSetting, AdminSettingViewModel>(_Context.AdminSettings.SingleOrDefault(ads => ads.Id == id));
        }

        public AdminSettingViewModel GetSetting(string setting)
        {
            return _Mapper.Map<AdminSetting, AdminSettingViewModel>(_Context.AdminSettings.Where(ads => ads.Name == setting).FirstOrDefault());
        }

        public AdminSettingViewModel Update(AdminSettingViewModel adminSetting)
        {
            AdminSetting currentAdminSetting = _Context.AdminSettings.SingleOrDefault(ads => ads.Id == adminSetting.Id);

            if (currentAdminSetting == null)
            {
                return null;
            }

            currentAdminSetting.Value = adminSetting.Value;

            try
            {
                _Context.SaveChanges();
                return _Mapper.Map<AdminSetting, AdminSettingViewModel>(currentAdminSetting);
            }
            catch
            {
                return null;
            }
        }
    }
}
