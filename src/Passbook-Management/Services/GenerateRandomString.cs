﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Services
{
    public class GenerateRandomString
    {
        private static string possibleChars = "0123456789";
        private static char[] possibleCharsArray = possibleChars.ToCharArray();
        private static int possibleCharsAvailable = possibleChars.Length;
        private static Random random = new Random();

        public static string StringNumber(int number)
        {
            var rBytes = new byte[number];
            random.NextBytes(rBytes);
            var rName = new char[number];
            while (number-- > 0)
            {
                rName[number] = possibleCharsArray[rBytes[number] % possibleCharsAvailable];
            }
            return new string(rName);
        }
    }
}
