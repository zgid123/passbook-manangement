﻿using AutoMapper;
using Passbook_Management.Extensions;
using Passbook_Management.Models.View_Models;

namespace Passbook_Management.Models
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<User, UserViewModel>()
                .ForMember(uvm => uvm.Uuid, um => um.MapFrom(u => u.Id));
            CreateMap<UserViewModel, User>()
                .ForMember(u => u.Id, um => um.MapFrom(uvm => uvm.Uuid));

            CreateMap<SavingAccountType, SavingAccountTypeViewModel>();
            CreateMap<SavingAccountTypeViewModel, SavingAccountType>();

            CreateMap<SavingAccount, SavingAccountViewModel>();
            CreateMap<SavingAccountViewModel, SavingAccount>();

            CreateMap<Pagination<SavingAccount>, Pagination<SavingAccountViewModel>>();

            CreateMap<AdminSetting, AdminSettingViewModel>();
            CreateMap<AdminSettingViewModel, AdminSetting>();

            CreateMap<BankAccount, BankAccountViewModel>();
            CreateMap<BankAccountViewModel, BankAccount>();

            CreateMap<TransactionHistory, TransactionHistoryViewModel>()
                .ForMember(thvm => thvm.TransactionType, thm => thm.MapFrom(th => ((TransactionHistoryViewModel.Types)th.TransactionType).ToValue()))
                .ForMember(thvm => thvm.AccountNumber, thm => thm.MapFrom(th => th.SavingAccount.BankAccount.Number))
                .ForMember(thvm => thvm.EmployeeName, thm => thm.MapFrom(th => th.Employee.User.Name));
            CreateMap<TransactionHistoryViewModel, TransactionHistory>();

            CreateMap<Employee, EmployeeViewModel>();
            CreateMap<EmployeeViewModel, Employee>();
        }
    }
}
