﻿using Passbook_Management.Models.Abstract_Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models
{
    public class TransactionHistory : Model
    {
        [Key]
        public int Id { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime RequestedAt { get; set; }

        public int TransactionType { get; set; }

        [MaxLength(50)]
        public string PersonName { get; set; }

        public long Amount { get; set; }

        public int? SavingAccountId { get; set; }

        public int EmployeeId { get; set; }

        [ForeignKey("SavingAccountId")]
        public SavingAccount SavingAccount { get; set; }

        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }
    }
}
