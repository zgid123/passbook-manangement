﻿using Passbook_Management.Models.Abstract_Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models
{
    public class BankAccount : Model
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(12), MinLength(12)]
        public string Number { get; set; }

        public long Balance { get; set; }

        public int AccountType { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public SavingAccount SavingAccount { get; set; }
    }
}
