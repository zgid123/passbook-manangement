﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models.View_Models
{
    public class UserViewModel
    {
        public string Address { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string PersonalIdentity { get; set; }
        public string PhoneNumber { get; set; }
        public string Role { get; set; }
        public string Uuid { get; set; }
    }
}