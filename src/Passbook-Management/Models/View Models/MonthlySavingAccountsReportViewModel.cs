﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models.View_Models
{
    public class MonthlySavingAccountsReportViewModel
    {
        public DateTime Date { get; set; }
        public int Creations { get; set; }
        public int Closures { get; set; }
        public int Total { get; set; }
    }
}
