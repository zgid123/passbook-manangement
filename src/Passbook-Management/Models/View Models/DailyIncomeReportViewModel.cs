﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models.View_Models
{
    public class DailyIncomeReportViewModel
    {
        public string SavingAccountType { get; set; }

        public long Income { get; set; }

        public long Disbursement { get; set; }

        public long Total { get; set; }
    }
}
