﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models.View_Models
{
    public class SavingAccountViewModel
    {
        public int Id { get; set; }
        public bool Active { get; set; }
        public BankAccountViewModel BankAccount { get; set; }
        public DateTime? ClosedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public EmployeeViewModel Employee { get; set; }
        public double InterestRate { get; set; }
        public double? Term { get; set; }
        public string Reason { get; set; }
        public SavingAccountTypeViewModel SavingAccountType { get; set; }
        public int Status { get; set; }
        public long Interest { get; set; }
    }
}
