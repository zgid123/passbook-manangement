﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models.View_Models
{
    public class WithdrawViewModel
    {
        public SavingAccountViewModel SavingAccount { get; set; }

        public long Value { get; set; }

        public string Trader { get; set; }
    }
}
