﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models.View_Models
{
    public class SavingAccountTypeViewModel
    {
        public int Id { get; set; }

        public double InterestRate { get; set; }

        public double? Term { get; set; }

        public string Name { get; set; }
    }
}
