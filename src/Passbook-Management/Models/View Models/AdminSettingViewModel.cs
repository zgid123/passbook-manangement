﻿using System.ComponentModel;

namespace Passbook_Management.Models.View_Models
{
    public class AdminSettingViewModel
    {
        public enum SettingNames
        {
            [Description("Tiền gửi tối thiểu")]
            MinimumDeposit,
            [Description("Tiền tạo tài khoản tối thiểu")]
            MinimumInitial,
            [Description("Số ngày tối thiểu được phép rút tiền")]
            MinimumDayWithdraw,
            [Description("Số tháng tối thiểu được tính lãi suất")]
            MinimumMonthRate
        };

        public int Id { get; set; }

        public string Name { get; set; }

        public long Value { get; set; }
    }
}
