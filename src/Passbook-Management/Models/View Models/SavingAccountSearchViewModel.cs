﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models.View_Models
{
    public class SavingAccountSearchViewModel
    {
        public string BankAccountNumber { get; set; }
        public string UserName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long FromBalance { get; set; }
        public long? ToBalance { get; set; }
        public string SavingAccountType { get; set; }
    }
}
