﻿using System;
using System.ComponentModel;

namespace Passbook_Management.Models.View_Models
{
    public class TransactionHistoryViewModel
    {
        public enum Types
        {
            [Description("Mở tài khoản tiết kiệm")]
            Creation,
            [Description("Cập nhật tài khoản tiết kiệm")]
            Update,
            [Description("Gửi tiền")]
            Deposit,
            [Description("Rút tiền")]
            Withdraw,
            [Description("Đóng sổ tiết kiệm")]
            Closure
        }

        public DateTime RequestedAt { get; set; }
        public string TransactionType { get; set; }
        public string PersonName { get; set; }
        public long Amount { get; set; }
        public string AccountNumber { get; set; }
        public string EmployeeName { get; set; }
    }
}
