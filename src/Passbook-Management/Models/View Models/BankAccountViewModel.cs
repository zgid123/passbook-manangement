﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models.View_Models
{
    public class BankAccountViewModel
    {
        public enum TypeAccountNames
        {
            [Description("Tài khoản tiết kiệm")]
            SavingAccount,
            [Description("Thẻ tín dụng")]
            BankCard
        };

        public string Number { get; set; }
        public long Balance { get; set; }
        public int TypeAccount { get; set; }
        public UserViewModel User { get; set; }
    }
}
