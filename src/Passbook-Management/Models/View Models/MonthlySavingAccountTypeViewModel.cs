﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models.View_Models
{
    public class MonthlySavingAccountTypeViewModel
    {
        public DateTime Date { get; set; }
        public string SavingAccountType { get; set; }
    }
}
