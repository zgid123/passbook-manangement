﻿using Passbook_Management.Models.Abstract_Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models
{
    public class AdminSetting : Model
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public long Value { get; set; }
    }
}
