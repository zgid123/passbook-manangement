﻿using Passbook_Management.Models.Abstract_Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Passbook_Management.Models
{
    public class SavingAccount : Model
    {
        [Key]
        public int Id { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ClosedAt { get; set; }

        public double InterestRate { get; set; }

        public double? Term { get; set; }

        public bool Active { get; set; }

        public string Reason { get; set; }

        public int Status { get; set; }

        public int BankAccountId { get; set; }

        public int SavingAccountTypeId { get; set; }

        public int EmployeeId { get; set; }

        [ForeignKey("BankAccountId")]
        public BankAccount BankAccount { get; set; }

        [ForeignKey("SavingAccountTypeId")]
        public SavingAccountType SavingAccountType { get; set; }

        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }

        public IList<TransactionHistory> TransactionHistories { get; set; }
    }
}
