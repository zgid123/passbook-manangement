﻿using Passbook_Management.Models.Abstract_Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models
{
    public class Employee : Model
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public IList<SavingAccount> SavingAccounts { get; set; }

        public IList<TransactionHistory> TransactionHistories { get; set; }
    }
}
