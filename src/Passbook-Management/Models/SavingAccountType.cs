﻿using Passbook_Management.Models.Abstract_Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Passbook_Management.Models
{
    public class SavingAccountType : Model
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        public double InterestRate { get; set; }

        public double? Term { get; set; }

        public IList<SavingAccount> SavingAccounts { get; set; }
    }
}
