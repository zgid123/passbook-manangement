﻿$(document).ready(function () {
    $(document).on('click', '.vertical-dropdown .dropdown', function () {
        var $this = $(this);
        var dropdown = $this[0];
        if (dropdown.classList.contains('active')) {
            dropdown.classList.remove('active');
            dropdown.parentNode.classList.remove('active');
        } else {
            dropdown.classList.add('active');
            dropdown.parentNode.classList.add('active');
        }
        $this.next().transition('slide down');
    });
});
