﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Passbook_Management.Models.View_Models;
using Microsoft.AspNetCore.Identity;
using Passbook_Management.Models;
using Microsoft.Extensions.Logging;
using AutoMapper;
using Passbook_Management.Services;

namespace Passbook_Management.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly ILogger _Logger;
        private readonly IMapper _Mapper;
        private readonly UserAccountService _UserAccountService;

        public AccountController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            ILoggerFactory loggerFactory, IMapper mapper)
        {
            _Logger = loggerFactory.CreateLogger<AccountController>();
            _Mapper = mapper;
            _UserAccountService = new UserAccountService(signInManager, userManager, mapper);
        }

        [HttpGet("{uuid}")]
        public async Task<IActionResult> Get(string uuid)
        {
            User user = await _UserAccountService.GetUserById(uuid);
            UserViewModel uvm = _Mapper.Map<User, UserViewModel>(user);
            if (uvm != null)
            {
                IList<string> roles = await _UserAccountService.GetRoles(user);
                uvm.Role = roles.ToList().FirstOrDefault();
            }
            return Json(uvm);
        }

        [HttpPost("login")]
        [AcceptVerbs("POST")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _UserAccountService.Login(model);
                if (result.Succeeded)
                {
                    _Logger.LogInformation(1, "User logged in.");
                    User user = await _UserAccountService.GetUserByEmail(model.Email);
                    return Ok(new
                    {
                        message = "Đăng nhập thành công",
                        authToken = user.Id
                    });
                }
                if (result.IsLockedOut)
                {
                    _Logger.LogWarning(2, "User account locked out.");
                    return Json(new { status = 401, message = "Tài khoản đã bị khóa." });
                }
                else
                {
                    _Logger.LogInformation(3, "Invalid login attempt.");
                    return Json(new { status = 401, message = "Email hoặc mật khẩu không đúng." });
                }
            }

            // If we got this far, something failed, redisplay form
            return Json(new { status = 400, message = "Thông tin không hợp lệ!" });
        }
    }
}
