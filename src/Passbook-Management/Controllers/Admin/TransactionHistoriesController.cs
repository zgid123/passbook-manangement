﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Passbook_Management.Models.View_Models;
using Passbook_Management.Data;
using Passbook_Management.Services;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Passbook_Management.Controllers.Admin
{
    [Route("api/transaction-histories")]
    public class TransactionHistoriesController : Controller
    {
        private TransactionHistoryService _TransactionHistoryService;

        public TransactionHistoriesController(IMapper mapper, PassbookManagementContext context)
        {
            _TransactionHistoryService = new TransactionHistoryService(mapper, context);
        }

        [HttpPost("get-all")]
        [AcceptVerbs("POST")]
        public IList<TransactionHistoryViewModel> GetAll([FromQuery] int page, [FromBody] TransactionHistorySearch search)
        {
            return _TransactionHistoryService.GetTransactionHistories(page, search);
        }

        [HttpPost("saving-accounts/{id}")]
        [AcceptVerbs("POST")]
        public IList<TransactionHistoryViewModel> Get(int id, [FromQuery] int page, [FromBody] TransactionHistorySearch search)
        {
            return _TransactionHistoryService.GetBySavingAccount(id, page, search);
        }
    }
}
