﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Passbook_Management.Models.View_Models;
using Passbook_Management.Services;
using Passbook_Management.Data;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Passbook_Management.Models;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;
using System;

namespace Passbook_Management.Controllers.Admin
{
    [EnableCors("AllowSpecificOrigin")]
    [Route("api/saving-accounts")]
    public class SavingAccountsController : Controller
    {
        private SavingAccountService _SavingAccountService;

        public SavingAccountsController(IMapper mapper, PassbookManagementContext context, SignInManager<User> signInManager, UserManager<User> userManager)
        {
            _SavingAccountService = new SavingAccountService(mapper, context, signInManager, userManager);
        }

        [HttpPost("get-all")]
        [AcceptVerbs("POST")]
        public List<SavingAccountViewModel> GetAll([FromQuery] int page, [FromBody] SavingAccountSearchViewModel savingAccountSearchViewModel)
        {
            return _SavingAccountService.GetAll(page, savingAccountSearchViewModel);
        }

        [HttpGet("{id}")]
        public SavingAccountViewModel Get(int id)
        {
            return _SavingAccountService.GetById(id);
        }

        [HttpPost("create")]
        [AcceptVerbs("POST")]
        public async Task<IActionResult> Create([FromBody] SavingAccountViewModel savingAccount)
        {
            var newSavingAccount = await _SavingAccountService.CreateSavingAccount(savingAccount);
            object json;

            if (newSavingAccount != null)
            {
                json = new { status = 200, message = "Tài khoản tiết kiệm đã được mở thành công!", data = newSavingAccount };
            }
            else
            {
                json = new { status = 400, message = "Thông tin không hợp lệ!" };
            }

            return Json(json);
        }

        [HttpPut("update")]
        [AcceptVerbs("PUT")]
        public IActionResult Update([FromBody] SavingAccountViewModel savingAccount)
        {
            var newSavingAccount = _SavingAccountService.UpdateSavingAccount(savingAccount);
            object json;

            if (newSavingAccount != null)
            {
                json = new { status = 200, message = "Tài khoản tiết kiệm đã được cập nhật thành công!", data = newSavingAccount };
            }
            else
            {
                json = new { status = 400, message = "Thông tin không hợp lệ!" };
            }

            return Json(json);
        }

        [HttpPut("deposit")]
        [AcceptVerbs("PUT")]
        public IActionResult Deposit([FromBody] DepositViewModel depositViewModel)
        {
            var currentSavingAccount = _SavingAccountService.Deposit(depositViewModel);
            object json;

            if (currentSavingAccount != null)
            {
                if (currentSavingAccount.Id > 0)
                {
                    json = new { status = 200, message = "Gửi tiền thành công!" };
                }
                else if (currentSavingAccount.Id == -1)
                {
                    json = new { status = 400, message = String.Format("Số tiền gửi tối thiểu là {0:n0}!", (long)currentSavingAccount.InterestRate) };
                }
                else if (currentSavingAccount.Id == -2)
                {
                    json = new { status = 400, message = "Số tiền gửi tối đa mỗi lần là 3,000,000,000!" };
                }
                else
                {
                    json = new { status = 400, message = "Gửi thêm tiền chỉ áp dụng cho loại không kỳ hạn!" };
                }
            }
            else
            {
                json = new { status = 400, message = "Thông tin không hợp lệ!" };
            }

            return Json(json);
        }

        [HttpPut("withdraw")]
        [AcceptVerbs("PUT")]
        public IActionResult Withdraw([FromBody] WithdrawViewModel withdrawViewModel)
        {
            var currentSavingAccount = _SavingAccountService.Withdraw(withdrawViewModel);
            object json;

            if (currentSavingAccount != null)
            {
                if (currentSavingAccount.Id > 0)
                {
                    json = new { status = 200, message = String.Format("Rút thành công {0:n0} VNĐ.", currentSavingAccount.BankAccount.Balance) };
                }
                else if (currentSavingAccount.Id == -1)
                {
                    json = new { status = 400, message = "Số tiền rút không được là số âm!" };
                }
                else if (currentSavingAccount.Id == -2)
                {
                    json = new { status = 400, message = String.Format("Cần đủ {0} ngày hoặc hơn để rút được tiền.", currentSavingAccount.Status) };
                }
                else
                {
                    json = new { status = 400, message = String.Format("Cần đủ {0} tháng hoặc hơn để rút được tiền.", currentSavingAccount.Status) };
                }
            }
            else
            {
                json = new { status = 400, message = "Thông tin không hợp lệ!" };
            }

            return Json(json);
        }
    }
}
