﻿using Microsoft.AspNetCore.Mvc;
using Passbook_Management.Models.View_Models;
using Passbook_Management.Services;
using Passbook_Management.Data;
using AutoMapper;
using Passbook_Management.Extensions;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Passbook_Management.Controllers.Admin
{
    [Route("api/admin-settings")]
    public class AdminSettingsController : Controller
    {
        private AdminSettingService _AdminSettingService;

        public AdminSettingsController(IMapper mapper, PassbookManagementContext context)
        {
            _AdminSettingService = new AdminSettingService(mapper, context);
        }

        [HttpGet]
        public List<AdminSettingViewModel> Get()
        {
            return _AdminSettingService.GetAll();
        }

        [HttpGet("{id}")]
        public AdminSettingViewModel Get(int id)
        {
            return _AdminSettingService.GetById(id);
        }

        [HttpGet("minimum-deposit")]
        public AdminSettingViewModel MinimumDeposit()
        {
            return _AdminSettingService.GetSetting(AdminSettingViewModel.SettingNames.MinimumDeposit.ToValue());
        }

        [HttpGet("minimum-initial")]
        public AdminSettingViewModel MinimumInitial()
        {
            return _AdminSettingService.GetSetting(AdminSettingViewModel.SettingNames.MinimumInitial.ToValue());
        }

        [HttpGet("minimum-day-withdraw")]
        public AdminSettingViewModel MinimumDayWithdraw()
        {
            return _AdminSettingService.GetSetting(AdminSettingViewModel.SettingNames.MinimumDayWithdraw.ToValue());
        }

        [HttpGet("minimum-month-rate")]
        public AdminSettingViewModel MinimumMonthRate()
        {
            return _AdminSettingService.GetSetting(AdminSettingViewModel.SettingNames.MinimumMonthRate.ToValue());
        }

        [HttpPut("update")]
        [AcceptVerbs("PUT")]
        public IActionResult Update([FromBody] AdminSettingViewModel adminSettingViewModel)
        {
            var newAdminSetting = _AdminSettingService.Update(adminSettingViewModel);
            object json;

            if (newAdminSetting != null)
            {
                json = new { status = 200, message = "Giá trị đã được cập nhật thành công!", data = newAdminSetting };
            }
            else
            {
                json = new { status = 400, message = "Thông tin không hợp lệ!" };
            }

            return Json(json);
        }
    }
}
