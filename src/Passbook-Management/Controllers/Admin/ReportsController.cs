﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Passbook_Management.Services;
using AutoMapper;
using Passbook_Management.Data;
using Passbook_Management.Models.View_Models;
using System;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Passbook_Management.Controllers.Admin
{
    [Route("api/reports")]
    public class ReportsController : Controller
    {
        private ReportService _ReportService;

        public ReportsController(IMapper mapper, PassbookManagementContext context)
        {
            _ReportService = new ReportService(mapper, context);
        }

        [HttpPost("daily-income")]
        public List<DailyIncomeReportViewModel> Daily([FromBody] DateTime date)
        {
            return _ReportService.DailyIncomeReport(date);
        }

        [HttpPost("monthly-saving-accounts")]
        public List<MonthlySavingAccountsReportViewModel> Get([FromBody] MonthlySavingAccountTypeViewModel monthlySavingAccountTypeViewModel)
        {
            return _ReportService.MonthlySavingAccountsReport(monthlySavingAccountTypeViewModel.Date, monthlySavingAccountTypeViewModel.SavingAccountType);
        }
    }
}
