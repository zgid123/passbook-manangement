﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Passbook_Management.Services;
using AutoMapper;
using Passbook_Management.Data;
using Passbook_Management.Models;
using Passbook_Management.Models.View_Models;

namespace Passbook_Management.Controllers.Admin
{
    [Route("api/saving-account-types")]
    public class SavingAccountTypesController : Controller
    {
        private SavingAccountTypeService _AccountTypeService;

        public SavingAccountTypesController(IMapper mapper, PassbookManagementContext context)
        {
            _AccountTypeService = new SavingAccountTypeService(mapper, context);
        }

        [HttpGet]
        public IList<SavingAccountTypeViewModel> Get()
        {
            return _AccountTypeService.GetSavingAccountTypes();
        }

        [HttpGet("{id}")]
        public SavingAccountTypeViewModel Get(int id)
        {
            return _AccountTypeService.GetById(id);
        }

        [HttpPost("create")]
        [AcceptVerbs("POST")]
        public IActionResult Post([FromBody] SavingAccountTypeViewModel savingAccountType)
        {
            var newSavingAccountType = _AccountTypeService.Create(savingAccountType);
            object json;

            if (newSavingAccountType != null)
            {
                if (newSavingAccountType.InterestRate > 0)
                {
                    json = new { status = 200, message = "Loại tài khoản tiết kiệm đã được tạo thành công!" };
                }
                else if (newSavingAccountType.InterestRate == -1)
                {
                    json = new { status = 400, message = "Loại tài khoản không kỳ hạn đã tồn tại!" };
                }
                else if (newSavingAccountType.InterestRate == -2)
                {
                    json = new { status = 400, message = "Lãi suất không được là số âm!" };
                }
                else
                {
                    json = new { status = 400, message = "Lãi suất theo kỳ hạn không được là số âm!" };
                }
            }
            else
            {
                json = new { status = 400, message = "Thông tin không hợp lệ!" };
            }

            return Json(json);
        }

        [HttpPut("update")]
        [AcceptVerbs("PUT")]
        public IActionResult Update([FromBody] SavingAccountTypeViewModel savingAccountType)
        {
            var newSavingAccountType = _AccountTypeService.Update(savingAccountType);
            object json;

            if (newSavingAccountType != null)
            {
                if (newSavingAccountType.InterestRate > 0)
                {
                    json = new { status = 200, message = "Loại tài khoản tiết kiệm đã được cập nhật thành công!" };
                }
                else if (newSavingAccountType.InterestRate == -1)
                {
                    json = new { status = 400, message = "Loại tài khoản không kỳ hạn không được sửa tên!" };
                }
                else if (newSavingAccountType.InterestRate == -2)
                {
                    json = new { status = 400, message = "Lãi suất không được là số âm!" };
                }
                else
                {
                    json = new { status = 400, message = "Lãi suất theo kỳ hạn không được là số âm!" };
                }
            }
            else
            {
                json = new { status = 400, message = "Thông tin không hợp lệ!" };
            }

            return Json(json);
        }
    }
}
