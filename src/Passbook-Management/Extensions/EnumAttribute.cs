﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Passbook_Management.Extensions
{
    public static class EnumAttribute
    {
        public static string ToValue(this Enum value)
        {
            var da = (DescriptionAttribute[])(value.GetType().GetField(value.ToString())).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return da.Length > 0 ? da[0].Description : value.ToString();
        }
    }
}
