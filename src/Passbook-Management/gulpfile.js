/// <binding AfterBuild='restore:app, restore:scss' Clean='clean:app' />
var gulp = require('gulp');
var clean = require('gulp-clean');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var typescript = require('gulp-typescript');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var concat = require('gulp-concat');

var paths = {
    wwwroot: {
        root: './wwwroot/',
        lib: './wwwroot/lib/',
        css: './wwwroot/css/',
        image: './wwwroot/images/',
        font: './wwwroot/fonts/',
        js: './wwwroot/js/',
        rootApp: ['./wwwroot/*.ts', './wwwroot/*.js', './wwwroot/*.map', './wwwroot/app/', './wwwroot/*.html']
    },
    app: 'App/**/*',
    css: 'Styles/**/*.scss',
    image: 'Images/**/*',
    js: 'Scripts/**/*.js',
    semantic: {
        components: 'node_modules/semantic-ui-css/components/',
        fonts: 'node_modules/semantic-ui-css/themes/default/assets/fonts/**/*'
    },
    kendoUI: {
        styles: 'Libs/Kendo UI/styles/**/*',
        js: 'Libs/Kendo UI/js/**/*',
        other: 'Libs/Kendo UI/other/**/*'
    },
    fontAwesome: {
        css: 'node_modules/font-awesome/css/',
        fonts: 'node_modules/font-awesome/fonts/**/*'
    },
    materialize: {
        components: 'node_modules/materialize-css/sass/',
        js: 'node_modules/materialize-css/js/',
        fonts: 'node_modules/materialize-css/fonts/**/*'
    }
};

gulp.task('default', function () { });

gulp.task('restore:core-js', function () {
    gulp.src([
        'node_modules/core-js/client/*.js'
    ]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + 'core-js'));
});
gulp.task('restore:zone.js', function () {
    gulp.src([
        'node_modules/zone.js/dist/*.js'
    ]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + 'zone.js'));
});
gulp.task('restore:reflect-metadata', function () {
    gulp.src([
        'node_modules/reflect-metadata/reflect.js'
    ]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + 'reflect-metadata'));
});
gulp.task('restore:systemjs', function () {
    gulp.src([
        'node_modules/systemjs/dist/*.js'
    ]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + 'systemjs'));
});
gulp.task('restore:rxjs', function () {
    gulp.src([
        'node_modules/rxjs/**/*.js'
    ]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + 'rxjs'));
});
gulp.task('restore:angular-in-memory-web-api', function () {
    gulp.src([
        'node_modules/angular-in-memory-web-api/**/*.js'
    ]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + 'angular-in-memory-web-api'));
});

gulp.task('restore:angular', function () {
    gulp.src([
        'node_modules/@angular/**/*.js'
    ]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + '@angular'));
});

gulp.task('restore:bootstrap', function () {
    gulp.src([
        'node_modules/bootstrap/dist/**/*.min.*'
    ]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + 'bootstrap'));
});

gulp.task('restore:materialize-css', function (done) {
    gulp.src([
        'Styles/missing-value/materialize.scss',
        paths.materialize.components + 'components/_color.scss',
        paths.materialize.components + 'components/_variables.scss',
        paths.materialize.components + 'components/_sideNav.scss',
        paths.materialize.components + 'components/_preloader.scss'
    ]).pipe(sourcemaps.init()).pipe(concat('materialize.scss')).pipe(sass()).on('error', sass.logError).pipe(cssmin())
      .pipe(rename({ extname: '.min.css' })).pipe(gulp.dest(paths.wwwroot.css + 'materialize-css/css')).on('end', done);
});

gulp.task('restore:materialize-fonts', function () {
    gulp.src([
        'node_modules/materialize-css/fonts/**/*'
    ]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.css + 'materialize-css/fonts'));
});

gulp.task('restore:tether', function () {
    gulp.src([
        'node_modules/tether/dist/**/*.min.*'
    ]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + 'tether'));
});

gulp.task('restore:lodash', function () {
    gulp.src(['node_modules/lodash/**/*']).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + 'lodash'));
});

gulp.task('restore:jquery', function () {
    gulp.src(['node_modules/jquery/dist/jquery.min.js']).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.lib + 'jquery'));
});

gulp.task('restore:app', function () {
    gulp.src([paths.app]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.root));
});

gulp.task('restore:js', function (done) {
    gulp.src([
        paths.semantic.components + 'transition.min.js',
        paths.semantic.components + 'dimmer.min.js',
        paths.semantic.components + 'dropdown.min.js',
        paths.semantic.components + 'modal.min.js',
        paths.materialize.js + 'global.js',
        paths.materialize.js + 'velocity.min.js',
        paths.materialize.js + 'hammer.min.js',
        paths.materialize.js + 'jquery.hammer.js',
        paths.materialize.js + 'transitions.js',
        paths.materialize.js + 'sideNav.js',
        paths.js
    ]).pipe(sourcemaps.init()).pipe(concat('site.js')).pipe(uglify()).pipe(rename({ extname: '.min.js' })).pipe(gulp.dest(paths.wwwroot.js)).on('end', done);
});

gulp.task('restore:scss', function (done) {
    gulp.src([paths.css, '!' + paths.css + 'missing-value/**/*']).pipe(sourcemaps.init()).pipe(concat('site.scss')).pipe(sass()).on('error', sass.logError).pipe(cssmin())
        .pipe(rename({ extname: '.min.css' })).pipe(gulp.dest(paths.wwwroot.css)).on('end', done);
});

gulp.task('restore:semantic-ui-css', function (done) {
    gulp.src([
        paths.semantic.components + 'icon.min.css',
        paths.semantic.components + 'menu.min.css',
        paths.semantic.components + 'transition.min.css',
        paths.semantic.components + 'dimmer.min.css',
        paths.semantic.components + 'dropdown.min.css',
        paths.semantic.components + 'modal.min.css',
        paths.semantic.components + 'segment.min.css',
        paths.semantic.components + 'loader.min.css'
    ]).pipe(sourcemaps.init()).pipe(concat('semantic-ui.min.css')).pipe(gulp.dest(paths.wwwroot.css + 'semantic/css/')).on('end', done);
});

gulp.task('restore:semantic-ui-font', function (done) {
    gulp.src([paths.semantic.fonts]).pipe(sourcemaps.init())
        .pipe(gulp.dest(paths.wwwroot.css + 'semantic/themes/default/assets/fonts/')).on('end', done);
});

gulp.task('restore:font-awesome-css', function (done) {
    gulp.src([paths.fontAwesome.css + 'font-awesome.min.css']).pipe(sourcemaps.init())
        .pipe(gulp.dest(paths.wwwroot.css + 'font-awesome/css/')).on('end', done);
});

gulp.task('restore:font-awesome-font', function (done) {
    gulp.src([paths.fontAwesome.fonts]).pipe(sourcemaps.init())
        .pipe(gulp.dest(paths.wwwroot.css + 'font-awesome/fonts/')).on('end', done);
});

gulp.task('restore:image', function (done) {
    gulp.src([paths.image]).pipe(sourcemaps.init()).pipe(gulp.dest(paths.wwwroot.image)).on('end', done);
});

gulp.task('restore:kendo-ui-css', function (done) {
    gulp.src([paths.kendoUI.styles]).pipe(sourcemaps.init()).pipe(concat('kendo.css'))
        .pipe(cssmin()).pipe(rename({ extname: '.min.css' })).pipe(gulp.dest(paths.wwwroot.lib + 'kendo-ui/styles')).on('end', done);
});

gulp.task('restore:kendo-ui-js', function (done) {
    gulp.src([paths.kendoUI.js]).pipe(sourcemaps.init())
        .pipe(gulp.dest(paths.wwwroot.lib + 'kendo-ui/js')).on('end', done);
});

gulp.task('restore:kendo-ui-styles', function (done) {
    gulp.src([paths.kendoUI.other]).pipe(sourcemaps.init())
        .pipe(gulp.dest(paths.wwwroot.lib + 'kendo-ui/styles')).on('end', done);
});

gulp.task('clean:app', function () {
    gulp.src(paths.wwwroot.rootApp, { read: false }).pipe(clean());
});

gulp.task('clean:css', function () {
    gulp.src(paths.wwwroot.css, { read: false }).pipe(clean());
});

gulp.task('clean:fonts', function () {
    gulp.src(paths.wwwroot.font, { read: false }).pipe(clean());
});

gulp.task('clean:js', function () {
    gulp.src(paths.wwwroot.js, { read: false }).pipe(clean());
});

gulp.task('clean:lib', function () {
    gulp.src(paths.wwwroot.lib, { read: false }).pipe(clean());
});

gulp.task('clean', ['clean:app', 'clean:css', 'clean:fonts', 'clean:js', 'clean:lib']);

gulp.task('restore', [
    'restore:core-js',
    'restore:zone.js',
    'restore:reflect-metadata',
    'restore:systemjs',
    'restore:rxjs',
    'restore:angular-in-memory-web-api',
    'restore:angular',
    'restore:bootstrap',
    'restore:materialize-css',
    'restore:materialize-fonts',
    'restore:tether',
    'restore:lodash',
    'restore:jquery',
    'restore:scss',
    'restore:image',
    'restore:js',
    'restore:semantic-ui-css',
    'restore:semantic-ui-font',
    'restore:kendo-ui-css',
    'restore:kendo-ui-js',
    'restore:kendo-ui-styles',
    'restore:font-awesome-css',
    'restore:font-awesome-font',
    'restore:app'
]);

gulp.task('watch', function () {
    gulp.watch([paths.app, paths.css, paths.js], [
        'restore:app',
        'restore:scss',
        'restore:js'
    ]);
});