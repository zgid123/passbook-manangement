﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NavbarComponent } from './navbar.component';
import { LoginComponent } from './login/login.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [NavbarComponent, LoginComponent],
    bootstrap: [NavbarComponent, LoginComponent]
})
export class NavbarModule { }