﻿import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { User } from '../../../shared/models/user';
import { AuthenticationService } from '../../../shared/services/authentication.service';
import { FlashService } from '../../../shared/components/flash/flash.service';

@Component({
    selector: 'login',
    moduleId: module.id,
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    model: any = {};

    constructor(private authenticationService: AuthenticationService, private flashService: FlashService,
        private router: Router) { }

    ngOnInit() {

    }

    login() {
        this.authenticationService.login(this.model.email, this.model.password, this.model.rememberMe)
            .subscribe(
            result => {
                switch (result.status) {
                    case 400:
                    case 401:
                        this.flashService.error(result.message);
                        break;
                    default:
                        $('.ui.dimmer.modals').remove();
                        this.flashService.success(result.message, true, true);
                        this.router.navigate(['admin-portal']);
                }
            },
            error => {
            });
    }
}