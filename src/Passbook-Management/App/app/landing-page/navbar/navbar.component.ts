﻿import { Component, OnInit, AfterViewInit, HostListener, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../shared/models/user';

@Component({
    selector: 'navbar',
    moduleId: module.id,
    templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
    public isFixed: boolean = false;
    user: User;

    constructor( @Inject(DOCUMENT) private document: Document, private router: Router, private route: ActivatedRoute) {
        this.user = this.route.snapshot.data['user'];
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        let loginButton = this.document.querySelector('section.navbar .login-button');

        if (loginButton) {
            loginButton.addEventListener('click', function () {
                $('.login-modal').modal('show');
            });
        }
    }

    @HostListener('window:scroll', []) onScroll(event: any) {
        let top = this.document.body.scrollTop;
        if (top > 20) {
            this.isFixed = true;
        } else {
            this.isFixed = false;
        }
    }
}