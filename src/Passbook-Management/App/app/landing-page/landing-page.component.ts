﻿import { Component } from '@angular/core';

import { PreLoaderService } from '../shared/components/pre-loader/pre-loader.service';

@Component({
    selector: 'my-app',
    moduleId: module.id,
    templateUrl: './landing-page.component.html'
})
export class LandingPageComponent {
    constructor(private preLoaderService: PreLoaderService) {
        this.preLoaderService.hide();
    }
}