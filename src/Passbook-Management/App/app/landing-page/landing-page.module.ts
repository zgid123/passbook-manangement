﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LandingPageComponent } from './landing-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FlashComponent } from '../shared/components/flash/flash.component';
import { PreLoaderComponent } from '../shared/components/pre-loader/pre-loader.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [LandingPageComponent, NavbarComponent, FlashComponent],
    bootstrap: [LandingPageComponent, NavbarComponent, FlashComponent]
})
export class LandingPageModule { }