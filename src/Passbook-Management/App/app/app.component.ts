﻿import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: '<loader></loader><router-outlet></router-outlet>'
})
export class AppComponent { }