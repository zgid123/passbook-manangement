﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LandingPageComponent } from './landing-page/landing-page.component';
import { AdminPortalComponent } from './admin-portal/admin-portal.component';
import { OverviewComponent } from './admin-portal/overview/overview.component';
import { SavingAccountComponent } from './admin-portal/saving-accounts/saving-account.component';
import { NewSavingAccountComponent } from './admin-portal/saving-accounts/create/saving-account-new.component';
import { SavingAccountDetailComponent } from './admin-portal/saving-accounts/detail/saving-account-detail.component';
import { EditSavingAccountComponent } from './admin-portal/saving-accounts/edit/saving-account-edit.component';
import { DepositSavingAccountComponent } from './admin-portal/saving-accounts/deposit/saving-account-deposit.component';
import { WithdrawSavingAccountComponent } from './admin-portal/saving-accounts/withdraw/saving-account-withdraw.component';
import { SavingAccountHistoryComponent } from './admin-portal/saving-accounts/history/saving-account-history.component';
import { SavingAccountTypeComponent } from './admin-portal/saving-account-types/saving-account-type.component';
import { NewSavingAccountTypeComponent } from './admin-portal/saving-account-types/create/saving-account-type-new.component';
import { EditSavingAccountTypeComponent } from './admin-portal/saving-account-types/edit/saving-account-type-edit.component';
import { AdminSettingComponent } from './admin-portal/admin-settings/admin-setting.component';
import { EditAdminSettingComponent } from './admin-portal/admin-settings/edit/admin-setting-edit.component';
import { TransactionHistoryComponent } from './admin-portal/transaction-histories/transaction-history.component';
import { DailyIncomeReportComponent } from './admin-portal/reports/daily-income/daily-income-report.component';
import { MonthlySavingAccountsReportComponent } from './admin-portal/reports/monthly-saving-accounts/monthly-saving-accounts-report.component';

import { AuthenticationResolveService } from './shared/services/authentication-resolve.service';
import { AuthenticationGuard } from './shared/services/authentication-guard.service';
import { SavingAccountResolveService } from './admin-portal/saving-accounts/saving-account-resolve.service';
import { SavingAccountDetailResolveService } from './admin-portal/saving-accounts/detail/saving-account-detail-resolve.service';
import { EditSavingAccountResolveService } from './admin-portal/saving-accounts/edit/saving-account-edit-resolve.service';
import { MinimumInitialResolveService } from './admin-portal/saving-accounts/shared/minimum-initial-resolve.service';
import { MinimumDepositResolveService } from './admin-portal/saving-accounts/deposit/minimum-deposit-resolve.service';
import { SavingAccountTransactionHistoryResolveService } from './admin-portal/saving-accounts/history/saving-account-transaction-history-resolve.service';
import { SavingAccountTypeResolveService } from './admin-portal/saving-account-types/saving-account-type-resolve.service';
import { EditSavingAccountTypeResolveService } from './admin-portal/saving-account-types/edit/saving-account-type-edit-resolve.service';
import { AdminSettingResolveService } from './admin-portal/admin-settings/admin-setting-resolve.service';
import { EditAdminSettingResolveService } from './admin-portal/admin-settings/edit/admin-setting-edit-resolve.service';
import { TransactionHistoryResolveService } from './admin-portal/transaction-histories/transaction-history-resolve.service';

const routes: Routes = [
    { path: '', component: LandingPageComponent, pathMatch: 'full', resolve: { user: AuthenticationResolveService } },
    {
        path: 'admin-portal', component: AdminPortalComponent, resolve: { user: AuthenticationResolveService }, canActivate: [AuthenticationGuard],
        children: [
            { path: '', component: OverviewComponent, pathMatch: 'full' },
            {
                path: 'passbooks', component: SavingAccountComponent, resolve: {
                    savingAccounts: SavingAccountResolveService,
                    savingAccountTypes: SavingAccountTypeResolveService,
                    adminSetting: MinimumInitialResolveService
                }
            },
            {
                path: 'passbooks/new', component: NewSavingAccountComponent, resolve: {
                    savingAccountTypes: SavingAccountTypeResolveService,
                    adminSetting: MinimumInitialResolveService
                }
            },
            {
                path: 'passbooks/:id', component: SavingAccountDetailComponent, resolve: {
                    savingAccount: SavingAccountDetailResolveService
                }
            },
            {
                path: 'passbooks/edit/:id', component: EditSavingAccountComponent, resolve: {
                    savingAccount: EditSavingAccountResolveService,
                    savingAccountTypes: SavingAccountTypeResolveService,
                    adminSetting: MinimumInitialResolveService
                }
            },
            {
                path: 'passbooks/:id/deposit', component: DepositSavingAccountComponent, resolve: {
                    savingAccount: SavingAccountDetailResolveService,
                    adminSetting: MinimumDepositResolveService
                }
            },
            {
                path: 'passbooks/:id/withdraw', component: WithdrawSavingAccountComponent, resolve: {
                    savingAccount: SavingAccountDetailResolveService
                }
            },
            {
                path: 'passbooks/:id/history', component: SavingAccountHistoryComponent, resolve: {
                    transactionHistories: SavingAccountTransactionHistoryResolveService,
                    savingAccount: SavingAccountDetailResolveService
                }
            },
            {
                path: 'passbook-types', component: SavingAccountTypeComponent, resolve: {
                    savingAccountTypes: SavingAccountTypeResolveService
                }
            },
            { path: 'passbook-types/new', component: NewSavingAccountTypeComponent },
            {
                path: 'passbook-types/edit/:id', component: EditSavingAccountTypeComponent, resolve: {
                    savingAccountType: EditSavingAccountTypeResolveService
                }
            },
            {
                path: 'admin-settings', component: AdminSettingComponent, resolve: {
                    adminSettings: AdminSettingResolveService
                }
            },
            {
                path: 'admin-settings/edit/:id', component: EditAdminSettingComponent, resolve: {
                    adminSetting: EditAdminSettingResolveService
                }
            },
            {
                path: 'transaction-histories', component: TransactionHistoryComponent, resolve: {
                    transactionHistories: TransactionHistoryResolveService
                }
            },
            { path: 'daily-income-report', component: DailyIncomeReportComponent },
            {
                path: 'monthly-saving-accounts-report', component: MonthlySavingAccountsReportComponent, resolve: {
                    savingAccountTypes: SavingAccountTypeResolveService
                }
            }
        ]
    },
    { path: '*', component: LandingPageComponent, resolve: { user: AuthenticationResolveService } }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }