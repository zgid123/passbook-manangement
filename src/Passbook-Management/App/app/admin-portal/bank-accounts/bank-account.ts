﻿import { User } from '../../shared/models/user';

export class BankAccount {
    number: string;
    balance: number;
    typeAccount: number;
    user: User;

    constructor(bankAccount: any = {}) {
        this.number = bankAccount.number;
        this.balance = bankAccount.balance;
        this.typeAccount = bankAccount.typeAccount;
        this.user = bankAccount.user;
    }
}