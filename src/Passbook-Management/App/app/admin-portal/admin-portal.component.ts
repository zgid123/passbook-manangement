﻿import { Component, HostListener, Inject, ElementRef } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Router, Resolve, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../shared/services/authentication.service';
import { FlashService } from '../shared/components/flash/flash.service';
import { User } from '../shared/models/user';

@Component({
    selector: 'my-app',
    moduleId: module.id,
    templateUrl: './admin-portal.component.html'
})
export class AdminPortalComponent {
    user: User;

    constructor( @Inject(DOCUMENT) private document: Document, private elementRef: ElementRef, private router: Router,
        private authenticationService: AuthenticationService, private flashService: FlashService, private route: ActivatedRoute) {
        this.user = this.route.snapshot.data['user'];
        if (!this.user || this.user.role != 'Admin') {
            this.flashService.error('Bạn cần đăng nhập trước khi tiếp tục.', true, true);
            this.router.navigate(['/']);
        }
    }

    activeDropdown(path: any[]) {
        for (const url in path) {
            if (this.router.isActive(this.router.serializeUrl(this.router.createUrlTree([path[url]])), true)) {
                return true;
            }
        }
        return false;
    }

    logout() {
        this.flashService.success("Bạn đã đăng xuất thành công.", true, true)
        this.authenticationService.logout();
    }
}