﻿import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AdminSettingService } from './admin-setting.service';

@Injectable()
export class AdminSettingResolveService implements Resolve<any> {
    constructor(private adminSettingService: AdminSettingService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return this.adminSettingService.getAdminSettings();
    }
}