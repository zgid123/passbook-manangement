﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AdminSetting } from './admin-setting';

@Component({
    selector: 'content',
    moduleId: module.id,
    templateUrl: './admin-setting.component.html'
})
export class AdminSettingComponent {
    adminSettings: AdminSetting[] = [];

    constructor(private router: Router, private route: ActivatedRoute) {
        this.adminSettings = this.route.snapshot.data['adminSettings'];
    }
}