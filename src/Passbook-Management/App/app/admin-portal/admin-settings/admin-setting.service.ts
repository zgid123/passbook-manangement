﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { AdminSetting } from './admin-setting';

@Injectable()
export class AdminSettingService {
    constructor(private http: Http, private router: Router) { }

    getAdminSettings() {
        return this.http.get('/api/admin-settings')
            .map((response: Response) => response.json());
    }

    getAdminSetting(id: number) {
        return this.http.get(`/api/admin-settings/${id}`)
            .map((response: Response) => response.json());
    }

    getMinimumDeposit() {
        return this.http.get('/api/admin-settings/minimum-deposit')
            .map((response: Response) => response.json());
    }

    getMinimumInitial() {
        return this.http.get('/api/admin-settings/minimum-initial')
            .map((response: Response) => response.json());
    }

    updateAdminSetting(adminSettingViewModel: AdminSetting) {
        return this.http.put('/api/admin-settings/update', adminSettingViewModel)
            .map((response: Response) => response.json());
    }
}
