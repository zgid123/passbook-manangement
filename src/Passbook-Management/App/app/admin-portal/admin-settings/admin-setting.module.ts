﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AdminSettingComponent } from './admin-setting.component';
import { FlashComponent } from '../../shared/components/flash/flash.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [AdminSettingComponent, FlashComponent],
    bootstrap: [AdminSettingComponent, FlashComponent]
})
export class AdminSettingModule { }