﻿export class AdminSetting {
    name: string;
    value: number;

    constructor(adminSetting: any = {}) {
        this.name = adminSetting.name;
        this.value = adminSetting.value;
    }
}