﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { EditAdminSettingComponent } from './admin-setting-edit.component';
import { FlashComponent } from '../../../shared/components/flash/flash.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [EditAdminSettingComponent, FlashComponent],
    bootstrap: [EditAdminSettingComponent, FlashComponent]
})
export class AdminSettingModule { }