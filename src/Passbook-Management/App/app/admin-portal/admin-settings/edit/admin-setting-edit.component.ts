﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';

import { FlashService } from '../../../shared/components/flash/flash.service';
import { AdminSettingService } from '../admin-setting.service';

import { maximumNumberValidator } from '../../../shared/directives/maximum-number.directive';
import { numberValidator } from '../../../shared/directives/number.directive';

import { AdminSetting } from '../admin-setting';

@Component({
    selector: 'content',
    moduleId: module.id,
    templateUrl: './admin-setting-edit.component.html'
})
export class EditAdminSettingComponent implements OnInit {
    adminSetting: AdminSetting;
    adminSettingForm: FormGroup;

    private validationMessages = {
        'value': {
            'required': 'Giá trị là thông tin bắt buộc.',
            'invalidNumber': 'Giá trị phải là số.',
            'invalidMaximumNumber': 'Giá trị tối đa là 3,000,000,000.'
        }
    };

    private formErrors = {
        'value': '',
    };

    private formGroup = {
        'value': '',
    }

    constructor(private router: Router, private route: ActivatedRoute, private flashService: FlashService, private fb: FormBuilder,
        private adminSettingService: AdminSettingService) {
        this.adminSetting = this.route.snapshot.data['adminSetting'];

        if (!this.adminSetting) {
            this.flashService.error('Cấu hình không tồn tại!', true, true);
            this.router.navigate(['/admin-portal/admin-settings']);
        }
    }

    ngOnInit() {
        this.adminSettingForm = this.fb.group({
            value: [this.adminSetting.value, [
                Validators.required,
                numberValidator(),
                maximumNumberValidator(3000000000)
            ]]
        });

        this.adminSettingForm.valueChanges.subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    submitForm() {
        if (this.adminSettingForm.invalid) {
            this.validateForm();
        } else {
            this.adminSetting.value = this.adminSettingForm.value.value;
            this.adminSettingService.updateAdminSetting(this.adminSetting).subscribe(result => {
                switch (result.status) {
                    case 200:
                        this.flashService.success(result.message, true, true);
                        this.router.navigate(['admin-portal/admin-settings']);
                        break;
                    default:
                        this.flashService.error(result.message);
                }
            });
        }
    }

    private onValueChanged(data?: any) {
        if (!this.adminSettingForm) { return; }

        const form = this.adminSettingForm;

        for (const field in this.formGroup) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && childControl.dirty && childControl.invalid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.dirty && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private validateForm() {
        if (!this.adminSettingForm) { return; }
        const form = this.adminSettingForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && !childControl.valid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private setErrorForControl(control: AbstractControl, field: string) {
        // clear previous error message (if any)
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
        }
    }
}