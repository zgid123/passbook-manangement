﻿export class TransactionHistorySearch {
    startDate: Date;
    endDate: Date;
    trader: string;

    constructor(transactionHistorySearch: any = {}) {
        this.startDate = transactionHistorySearch.startDate;
        this.endDate = transactionHistorySearch.endDate;
        this.trader = transactionHistorySearch.trader || '';
    }
}