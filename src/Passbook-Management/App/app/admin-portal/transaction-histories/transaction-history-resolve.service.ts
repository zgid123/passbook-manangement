﻿import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TransactionHistoryService } from './transaction-history.service';

@Injectable()
export class TransactionHistoryResolveService implements Resolve<any> {
    constructor(private transactionHistoryService: TransactionHistoryService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return this.transactionHistoryService.getTransactionHistories();
    }
}