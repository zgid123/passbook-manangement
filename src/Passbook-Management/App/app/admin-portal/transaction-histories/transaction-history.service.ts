﻿import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { TransactionHistory } from './transaction-history';
import { TransactionHistorySearch } from './transaction-history-search';

@Injectable()
export class TransactionHistoryService {
    constructor(private http: Http, private router: Router) { }

    getTransactionHistories(page: number = 1, transactionHistorySearch: TransactionHistorySearch = new TransactionHistorySearch()) {
        return this.http.post(`/api/transaction-histories/get-all?page=${page}`, transactionHistorySearch)
            .map((response: Response) => response.json());
    }

    getTransactionHistoriesBySavingAccount(id: number, page: number = 1, transactionHistorySearch: TransactionHistorySearch = new TransactionHistorySearch()) {
        return this.http.post(`/api/transaction-histories/saving-accounts/${id}?page=${page}`, transactionHistorySearch)
            .map((response: Response) => response.json());
    }
}
