﻿export class TransactionHistory {
    requestedAt: Date;
    requestedAtString: string;
    transactionType: string;
    personName: string;
    amount: number;
    accountNumber: string;
    employeeName: string;

    constructor(transactionHistory: any = {}) {
        this.requestedAt = transactionHistory.requestedAt;
        this.transactionType = transactionHistory.transactionType;
        this.personName = transactionHistory.personName;
        this.amount = transactionHistory.amount;
        this.accountNumber = transactionHistory.accountNumber;
        this.employeeName = transactionHistory.employeeName;
    }
}