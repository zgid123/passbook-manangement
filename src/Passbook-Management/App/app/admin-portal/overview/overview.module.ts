﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { OverviewComponent } from './overview.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [OverviewComponent],
    bootstrap: [OverviewComponent]
})
export class OverviewModule { }