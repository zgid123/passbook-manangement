﻿import { User } from '../../shared/models/user';

export class Employee {
    user: User;

    constructor(user: User) {
        this.user = user;
    }
}