﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AdminPortalComponent } from './admin-portal.component';
import { FlashComponent } from '../shared/components/flash/flash.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [AdminPortalComponent, FlashComponent],
    bootstrap: [AdminPortalComponent, FlashComponent]
})
export class AdminPortalModule { }