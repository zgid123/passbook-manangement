﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SavingAccountComponent } from './saving-account.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [SavingAccountComponent],
    bootstrap: [SavingAccountComponent]
})
export class SavingAccountModule { }