﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NewSavingAccountComponent } from './saving-account-new.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [NewSavingAccountComponent],
    bootstrap: [NewSavingAccountComponent]
})
export class NewSavingAccountModule { }