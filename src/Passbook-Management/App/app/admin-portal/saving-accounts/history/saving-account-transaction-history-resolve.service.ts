﻿import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TransactionHistoryService } from '../../transaction-histories/transaction-history.service';

@Injectable()
export class SavingAccountTransactionHistoryResolveService implements Resolve<any> {
    constructor(private transactionHistoryService: TransactionHistoryService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return this.transactionHistoryService.getTransactionHistoriesBySavingAccount(route.params['id']);
    }
}