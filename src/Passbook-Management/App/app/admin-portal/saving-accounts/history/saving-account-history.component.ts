﻿import { Component, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { TransactionHistory } from '../../transaction-histories/transaction-history';
import { SavingAccount } from '../saving-account';
import { TransactionHistorySearch } from '../../transaction-histories/transaction-history-search';

import { DateFormatService } from '../../../shared/services/date-format.service';
import { TransactionHistoryService } from '../../transaction-histories/transaction-history.service';
import { FlashService } from '../../../shared/components/flash/flash.service';

@Component({
    selector: 'content',
    moduleId: module.id,
    templateUrl: './saving-account-history.component.html'
})
export class SavingAccountHistoryComponent implements AfterViewInit {
    transactionHistories: TransactionHistory[] = [];
    savingAccount: SavingAccount;
    transactionHistorySearch: TransactionHistorySearch = new TransactionHistorySearch();
    page: number = 1;

    constructor(private router: Router, private route: ActivatedRoute, private dateFormatService: DateFormatService,
        private transactionHistoryService: TransactionHistoryService, private flashService: FlashService) {
        this.transactionHistories = this.route.snapshot.data['transactionHistories'];
        this.savingAccount = this.route.snapshot.data['savingAccount'];

        if (!this.savingAccount) {
            this.flashService.error('Không tồn tại tài khoản tiết kiệm.', true, true);
            this.router.navigate(['admin-portal/passbooks']);
        }

        if (this.transactionHistories) {
            for (const transactionHistory in this.transactionHistories) {
                this.transactionHistories[transactionHistory].requestedAtString = this.dateFormatService.formatDate(this.transactionHistories[transactionHistory].requestedAt);
            }
        }
    }

    ngAfterViewInit() {
        let that = this;
        let $grid = $('#transaction-history-list');

        $grid.kendoGrid({
            dataSource: {
                data: this.transactionHistories
            },
            sortable: {
                mode: 'multiple',
                allowUnsort: true
            },
            scrollable: true,
            groupable: true,
            height: '496px',
            columns: [
                {
                    field: 'requestedAtString',
                    title: 'Ngày giao dịch',
                    minResizableWidth: 140
                },
                { field: 'transactionType', title: 'Công việc', minResizableWidth: 140 },
                { field: 'personName', title: 'Người giao dịch', minResizableWidth: 140 },
                { field: 'accountNumber', title: 'Số tài khoản', minResizableWidth: 140 },
                { field: 'employeeName', title: 'Nhân viên', minResizableWidth: 140 },
                { field: 'amount', title: 'Số tiền', format: '{0:n0} VNĐ', minResizableWidth: 140 },
            ]
        }).data('kendoGrid');

        $('section.histories-saving-account .search-button').sideNav({ edge: 'right' });

        $('.k-grid-content.k-auto-scrollable').scroll(function () {
            let $this = $(this);
            if (that.page != -1) {
                if ($this.scrollTop() + $this.height() >= $('.k-grid-content.k-auto-scrollable > table').height()) {
                    that.transactionHistoryService.getTransactionHistoriesBySavingAccount(that.savingAccount.id, ++that.page, that.transactionHistorySearch).subscribe(result => {
                        if (result.length) {
                            for (const transactionHistory in result) {
                                result[transactionHistory].requestedAtString = that.dateFormatService.formatDate(result[transactionHistory].requestedAt);
                                that.transactionHistories.push(result[transactionHistory]);
                            }
                            $grid.data('kendoGrid').dataSource.data(that.transactionHistories);
                        } else {
                            that.page = -1;
                        }
                    });
                }
            }
        });

        $('.date-picker').kendoDatePicker({
            parseFormats: ['dd/MM/yyyy'],
            format: 'dd/MM/yyyy'
        });

        $("#start-date, #end-date").on("click", function () {
            $(this).data("kendoDatePicker").open();
        });
    }

    private search() {
        this.page = 1;
        this.transactionHistorySearch.startDate = $('#start-date').data('kendoDatePicker').value();
        this.transactionHistorySearch.endDate = $('#end-date').data('kendoDatePicker').value();
        this.transactionHistoryService.getTransactionHistoriesBySavingAccount(this.savingAccount.id, this.page, this.transactionHistorySearch).subscribe(result => {
            this.transactionHistories = [];
            for (const transactionHistory in result) {
                result[transactionHistory].requestedAtString = this.dateFormatService.formatDate(result[transactionHistory].requestedAt);
                this.transactionHistories.push(result[transactionHistory]);
            }

            let $grid = $('#transaction-history-list').data('kendoGrid');

            $grid.dataSource.data(this.transactionHistories);
        });
    }

    private clearSearch() {
        this.transactionHistorySearch = new TransactionHistorySearch();
        $('#start-date').data('kendoDatePicker').value('');
        $('#end-date').data('kendoDatePicker').value('');
        this.page = 1;
        this.transactionHistoryService.getTransactionHistoriesBySavingAccount(this.savingAccount.id, this.page, this.transactionHistorySearch).subscribe(result => {
            this.transactionHistories = [];
            for (const transactionHistory in result) {
                result[transactionHistory].requestedAtString = this.dateFormatService.formatDate(result[transactionHistory].requestedAt);
                this.transactionHistories.push(result[transactionHistory]);
            }
        });
        $('#transaction-history-list').data('kendoGrid').dataSource.read();
    }
}
