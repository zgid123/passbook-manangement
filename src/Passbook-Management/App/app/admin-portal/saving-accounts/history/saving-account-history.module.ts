﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SavingAccountHistoryComponent } from './saving-account-history.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [SavingAccountHistoryComponent],
    bootstrap: [SavingAccountHistoryComponent]
})
export class SavingAccountHistoryModule { }