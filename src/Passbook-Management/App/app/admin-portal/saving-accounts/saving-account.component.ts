﻿import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import * as _ from 'lodash';

import { SavingAccountService } from './saving-account.service';

import { SavingAccount } from './saving-account';
import { SavingAccountSearchViewModel } from './saving-account-search-view-model';
import { SavingAccountType } from '../saving-account-types/saving-account-type';
import { AdminSetting } from '../admin-settings/admin-setting';

import { DateFormatService } from '../../shared/services/date-format.service';

@Component({
    selector: 'content',
    moduleId: module.id,
    templateUrl: './saving-account.component.html'
})
export class SavingAccountComponent implements OnInit, AfterViewInit {
    savingAccounts: SavingAccount[] = [];
    savingAccountTypes: SavingAccountType[] = [];
    savingAccountSearchViewModel: SavingAccountSearchViewModel = new SavingAccountSearchViewModel();
    savingAccountType: SavingAccountType;
    adminSetting: AdminSetting;
    page: number = 1;

    constructor( @Inject(DOCUMENT) private document: Document, private router: Router, private route: ActivatedRoute,
        private savingAccountService: SavingAccountService, private dateFormatService: DateFormatService) {
        this.savingAccounts = this.route.snapshot.data['savingAccounts'];
        this.savingAccountTypes = this.route.snapshot.data['savingAccountTypes'];
        this.adminSetting = this.route.snapshot.data['adminSetting'];

        if (this.savingAccounts) {
            for (const savingAccount in this.savingAccounts) {
                this.savingAccounts[savingAccount].index = parseInt(savingAccount) + 1;
                this.savingAccounts[savingAccount].createdAtString = this.dateFormatService.formatDate(this.savingAccounts[savingAccount].createdAt);
            }
        }
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        let that = this;
        let $grid = $('#saving-account-list');

        $grid.kendoGrid({
            dataSource: {
                data: this.savingAccounts
            },
            sortable: {
                mode: 'multiple',
                allowUnsort: true
            },
            scrollable: true,
            groupable: true,
            height: '496px',
            columns: [
                { field: 'index', title: 'STT', width: 60 },
                { field: 'bankAccount.number', title: 'Số tài khoản', minResizableWidth: 140 },
                { field: 'savingAccountType.name', title: 'Loại tài khoản', minResizableWidth: 140 },
                { field: 'bankAccount.user.name', title: 'Tên khách hàng', minResizableWidth: 140 },
                {
                    field: 'createdAtString',
                    title: 'Ngày mở',
                    minResizableWidth: 140
                },
                { field: 'bankAccount.balance', title: 'Số dư', format: '{0:n0} VNĐ', minResizableWidth: 140 },
                { field: 'active', title: 'Tình trạng', template: "#= active ? 'Mở' : 'Đóng' #", minResizableWidth: 140 },
                {
                    command: [
                        {
                            text: ' ',
                            template: '<a class="k-button k-button-icontext k-grid- view-detail"><span class="fa fa-eye"></span></a>'
                        },
                        {
                            text: ' ',
                            template: '<a class="k-button k-button-icontext k-grid- edit-detail"><span class="fa fa-pencil"></span></a>'
                        },
                        {
                            text: ' ',
                            template: '<a class="k-button k-button-icontext k-grid- deposit"><span class="fa fa-sign-in"></span></a>'
                        },
                        {
                            text: ' ',
                            template: '<a class="k-button k-button-icontext k-grid- withdraw"><span class="fa fa-sign-out"></span></a>'
                        },
                        {
                            text: ' ',
                            template: '<a class="k-button k-button-icontext k-grid- history"><span class="fa fa-file-text"></span></a>'
                        }
                    ],
                    title: ' ',
                    width: 90
                }
            ]
        }).data('kendoGrid');

        $grid.kendoTooltip({
            filter: ".k-button.view-detail",
            content: 'Xem chi tiết'
        });

        $grid.kendoTooltip({
            filter: ".k-button.edit-detail",
            content: 'Chỉnh sửa'
        });

        $grid.kendoTooltip({
            filter: ".k-button.deposit",
            content: 'Gửi tiền'
        });

        $grid.kendoTooltip({
            filter: ".k-button.withdraw",
            content: 'Rút tiền'
        });

        $grid.kendoTooltip({
            filter: ".k-button.history",
            content: 'Xem lịch sử giao dịch'
        });

        this.removeDepositButton();
        this.removeAllButton();

        $('#saving-account-list').on('click', '.k-button.k-button-icontext.view-detail, .k-button.k-button-icontext.edit-detail, .k-button.k-button-icontext.deposit, .k-button.k-button-icontext.withdraw, .k-button.k-button-icontext.history', function (event: any) {
            let $grid = $('#saving-account-list').data('kendoGrid');
            let dataItem = $grid.dataItem($(this).closest("tr"));
            let className = event.currentTarget.className;
            if (className.includes('view-detail')) {
                that.router.navigate([`admin-portal/passbooks/${dataItem.id}`]);
            } else if (className.includes('edit-detail')) {
                that.router.navigate([`admin-portal/passbooks/edit/${dataItem.id}`]);
            } else if (className.includes('deposit')) {
                that.router.navigate([`admin-portal/passbooks/${dataItem.id}/deposit`]);
            } else if (className.includes('withdraw')) {
                that.router.navigate([`admin-portal/passbooks/${dataItem.id}/withdraw`]);
            } else {
                that.router.navigate([`admin-portal/passbooks/${dataItem.id}/history`]);
            }
        });

        $('section.passbooks .search-button').sideNav({ edge: 'right' });

        $('.k-grid-content.k-auto-scrollable').scroll(function () {
            let $this = $(this);
            if (that.page != -1) {
                if ($this.scrollTop() + $this.height() >= $('.k-grid-content.k-auto-scrollable > table').height()) {
                    that.savingAccountService.getSavingAccounts(++that.page, that.savingAccountSearchViewModel).subscribe(result => {
                        if (result.length) {
                            for (const savingAccount in result) {
                                result[savingAccount].index = (that.page - 1) * 12 + parseInt(savingAccount) + 1;
                                result[savingAccount].createdAtString = that.dateFormatService.formatDate(result[savingAccount].createdAt);
                                that.savingAccounts.push(result[savingAccount]);
                            }
                            $grid.data('kendoGrid').dataSource.data(that.savingAccounts);
                            that.removeDepositButton();
                            that.removeAllButton();
                        } else {
                            that.page = -1;
                        }
                    });
                }
            }
        });

        $('.date-picker').kendoDatePicker({
            parseFormats: ['dd/MM/yyyy'],
            format: 'dd/MM/yyyy'
        });

        $("#start-date, #end-date").on("click", function () {
            $(this).data("kendoDatePicker").open();
        });
    }

    private search() {
        this.page = 1;
        this.savingAccountSearchViewModel.startDate = $('#start-date').data('kendoDatePicker').value();
        this.savingAccountSearchViewModel.endDate = $('#end-date').data('kendoDatePicker').value();
        this.savingAccountService.getSavingAccounts(this.page, this.savingAccountSearchViewModel).subscribe(result => {
            this.savingAccounts = [];
            for (const savingAccount in result) {
                result[savingAccount].index = parseInt(savingAccount) + 1;
                result[savingAccount].createdAtString = this.dateFormatService.formatDate(result[savingAccount].createdAt);
                this.savingAccounts.push(result[savingAccount]);
            }

            let $grid = $('#saving-account-list').data('kendoGrid');

            $grid.dataSource.data(this.savingAccounts);

            this.removeDepositButton();
            this.removeAllButton();
        });
    }

    private clearSearch() {
        this.savingAccountSearchViewModel = new SavingAccountSearchViewModel();
        $('#saving-account-list').data('kendoGrid').dataSource.read();
        $('#start-date').data('kendoDatePicker').value('');
        $('#end-date').data('kendoDatePicker').value('');
        this.page = 1;
        this.savingAccountService.getSavingAccounts(this.page, this.savingAccountSearchViewModel).subscribe(result => {
            this.savingAccounts = [];
            for (const savingAccount in result) {
                result[savingAccount].index = parseInt(savingAccount) + 1;
                result[savingAccount].createdAtString = this.dateFormatService.formatDate(result[savingAccount].createdAt);
                this.savingAccounts.push(result[savingAccount]);
            }
            this.removeDepositButton();
            this.removeAllButton();
        });
    }

    private removeDepositButton() {
        _.each($('.k-button.k-button-icontext.deposit'), function (button) {
            let $grid = $('#saving-account-list').data('kendoGrid');
            let dataItem = $grid.dataItem($(button).closest("tr"));
            if (dataItem.savingAccountType.name != 'Không kỳ hạn') {
                button.remove();
            }
        });
    }

    private removeAllButton() {
        _.each($('.k-button.k-button-icontext.deposit, .k-button.k-button-icontext.withdraw'), function (button) {
            let $grid = $('#saving-account-list').data('kendoGrid');
            let dataItem = $grid.dataItem($(button).closest("tr"));
            if (!dataItem.active) {
                button.remove();
            }
        });
    }
}
