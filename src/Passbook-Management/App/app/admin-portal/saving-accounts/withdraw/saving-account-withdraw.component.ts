﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';

import { FlashService } from '../../../shared/components/flash/flash.service';
import { SavingAccountService } from '../saving-account.service';

import { maximumNumberValidator } from '../../../shared/directives/maximum-number.directive';
import { numberValidator } from '../../../shared/directives/number.directive';
import { minimumNumberValidator } from '../../../shared/directives/minimum-number.directive';
import { blankStringValidator } from '../../../shared/directives/blank-string.directive';

import { WithdrawViewModel } from './withdraw-view-model';
import { AdminSetting } from '../../admin-settings/admin-setting';
import { Employee } from '../../employees/employee';
import { User } from '../../../shared/models/user';

@Component({
    selector: 'content',
    moduleId: module.id,
    templateUrl: './saving-account-withdraw.component.html'
})
export class WithdrawSavingAccountComponent implements OnInit {
    withdrawViewModel: WithdrawViewModel = new WithdrawViewModel();
    withdrawForm: FormGroup;

    private validationMessages = {
        'value': {
            'required': 'Số tiền rút là thông tin bắt buộc.',
            'invalidNumber': 'Số tiền rút phải là số.',
            'invalidMaximumNumber': 'Số tiền rút tối đa là 3,000,000,000.'
        },
        'trader': {
            'required': 'Tên người giao dịch là thông tin bắt buộc.',
            'blankString': 'Tên người giao dich không được rỗng.'
        }
    };

    private formErrors = {
        'value': '',
        'trader': ''
    };

    private formGroup = {
        'value': '',
        'trader': ''
    }

    constructor(private router: Router, private route: ActivatedRoute, private flashService: FlashService, private fb: FormBuilder,
        private savingAccountService: SavingAccountService) {
        this.withdrawViewModel.savingAccount = this.route.snapshot.data['savingAccount'];

        if (!this.withdrawViewModel) {
            this.flashService.error('Tài khoản tiết kiệm không tồn tại.');
            this.router.navigate(['admin-portal/passbooks']);
        }

        if (!this.withdrawViewModel.savingAccount.active) {
            this.flashService.error('Chỉ được rút tiền cho tài khoản đang mở.');
            this.router.navigate([`admin-portal/passbooks/${this.withdrawViewModel.savingAccount.id}`]);
        }

        this.validationMessages.value.invalidMaximumNumber = `Số tiền rút tối đa là ${(this.withdrawViewModel.savingAccount.bankAccount.balance + this.withdrawViewModel.savingAccount.interest).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}.`;
    }

    ngOnInit() {
        this.withdrawForm = this.fb.group({
            value: [this.withdrawViewModel.savingAccount.savingAccountType.name == 'Không kỳ hạn' ? this.withdrawViewModel.value : 0, [
                Validators.required,
                numberValidator(),
                maximumNumberValidator(this.withdrawViewModel.savingAccount.bankAccount.balance + this.withdrawViewModel.savingAccount.interest)
            ]],
            trader: [this.withdrawViewModel.trader, [
                Validators.required,
                blankStringValidator()
            ]]
        });

        this.withdrawForm.valueChanges.subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    submitForm() {
        if (this.withdrawForm.invalid) {
            this.validateForm();
        } else {
            this.withdrawViewModel.value = this.withdrawForm.value.value;
            this.withdrawViewModel.trader = this.withdrawForm.value.trader;
            this.withdrawViewModel.savingAccount.employee = new Employee(new User({ uuid: localStorage.getItem('currentUser') }));
            this.savingAccountService.withdraw(this.withdrawViewModel).subscribe(result => {
                switch (result.status) {
                    case 200:
                        this.flashService.success(result.message, true, true);
                        this.router.navigate([`admin-portal/passbooks/${this.withdrawViewModel.savingAccount.id}`]);
                        break;
                    default:
                        this.flashService.error(result.message);
                }
            });
        }
    }

    private onValueChanged(data?: any) {
        if (!this.withdrawForm) { return; }

        const form = this.withdrawForm;

        for (const field in this.formGroup) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && childControl.dirty && childControl.invalid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.dirty && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private validateForm() {
        if (!this.withdrawForm) { return; }
        const form = this.withdrawForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && !childControl.valid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private setErrorForControl(control: AbstractControl, field: string) {
        // clear previous error message (if any)
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
        }
    }
}