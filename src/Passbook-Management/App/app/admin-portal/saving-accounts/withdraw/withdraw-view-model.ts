﻿import { SavingAccount } from '../saving-account';

export class WithdrawViewModel {
    savingAccount: SavingAccount;
    value: number;
    trader: string;

    constructor(withdrawViewModel: any = {}) {
        this.savingAccount = withdrawViewModel.savingAccount;
        this.value = withdrawViewModel.value;
        this.trader = withdrawViewModel.trader;
    }
}