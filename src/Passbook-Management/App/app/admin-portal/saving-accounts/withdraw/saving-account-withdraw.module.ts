﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { WithdrawSavingAccountComponent } from './saving-account-withdraw.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [WithdrawSavingAccountComponent],
    bootstrap: [WithdrawSavingAccountComponent]
})
export class WithdrawSavingAccountModule { }