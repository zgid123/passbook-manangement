﻿export class SavingAccountSearchViewModel {
    bankAccountNumber: string;
    userName: string;
    startDate: Date;
    endDate: Date;
    fromBalance: number;
    toBalance: number;
    savingAccountType: string;

    constructor(savingAccountSearchViewModel: any = {}) {
        this.bankAccountNumber = savingAccountSearchViewModel.bankAccountNumber || '';
        this.userName = savingAccountSearchViewModel.userName || '';
        this.fromBalance = savingAccountSearchViewModel.fromBalance;
        this.toBalance = savingAccountSearchViewModel.toBalance;
        this.startDate = savingAccountSearchViewModel.startDate;
        this.endDate = savingAccountSearchViewModel.endDate;
        this.savingAccountType = savingAccountSearchViewModel.savingAccountType || '';
    }
}