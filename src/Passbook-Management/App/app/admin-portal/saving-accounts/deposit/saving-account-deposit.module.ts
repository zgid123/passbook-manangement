﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DepositSavingAccountComponent } from './saving-account-deposit.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [DepositSavingAccountComponent],
    bootstrap: [DepositSavingAccountComponent]
})
export class DepositSavingAccountModule { }