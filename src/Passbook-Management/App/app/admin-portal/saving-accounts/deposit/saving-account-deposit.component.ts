﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';

import { FlashService } from '../../../shared/components/flash/flash.service';
import { SavingAccountService } from '../saving-account.service';

import { maximumNumberValidator } from '../../../shared/directives/maximum-number.directive';
import { numberValidator } from '../../../shared/directives/number.directive';
import { minimumNumberValidator } from '../../../shared/directives/minimum-number.directive';
import { blankStringValidator } from '../../../shared/directives/blank-string.directive';

import { DepositViewModel } from './deposit-view-model';
import { AdminSetting } from '../../admin-settings/admin-setting';
import { Employee } from '../../employees/employee';
import { User } from '../../../shared/models/user';

@Component({
    selector: 'content',
    moduleId: module.id,
    templateUrl: './saving-account-deposit.component.html'
})
export class DepositSavingAccountComponent implements OnInit {
    depositViewModel: DepositViewModel = new DepositViewModel();
    adminSetting: AdminSetting;
    depositForm: FormGroup;

    private validationMessages = {
        'value': {
            'required': 'Số tiền gửi là thông tin bắt buộc.',
            'invalidNumber': 'Số tiền gửi phải là số.',
            'invalidMaximumNumber': 'Số tiền gửi tối đa là 3,000,000,000.',
            'invalidMinimumNumber': 'Số tiền gửi tối thiểu là 100,000.'
        },
        'trader': {
            'required': 'Tên người giao dịch là thông tin bắt buộc.',
            'blankString': 'Tên người giao dich không được rỗng.'
        }
    };

    private formErrors = {
        'value': '',
        'trader': ''
    };

    private formGroup = {
        'value': '',
        'trader': ''
    }

    constructor(private router: Router, private route: ActivatedRoute, private flashService: FlashService, private fb: FormBuilder,
        private savingAccountService: SavingAccountService) {
        this.depositViewModel.savingAccount = this.route.snapshot.data['savingAccount'];

        if (!this.depositViewModel) {
            this.flashService.error('Tài khoản tiết kiệm không tồn tại.');
            this.router.navigate(['admin-portal/passbooks']);
        }

        if (this.depositViewModel.savingAccount.savingAccountType.name != 'Không kỳ hạn') {
            this.flashService.error('Chỉ cho phép gửi tiền đối với tài khoản không kỳ hạn.');
            this.router.navigate([`admin-portal/passbooks/${this.depositViewModel.savingAccount.id}`]);
        }

        if (!this.depositViewModel.savingAccount.active) {
            this.flashService.error('Chỉ được gửi tiền cho tài khoản đang mở.');
            this.router.navigate([`admin-portal/passbooks/${this.depositViewModel.savingAccount.id}`]);
        }

        this.adminSetting = this.route.snapshot.data['adminSetting'];

        if (!this.adminSetting) {
            this.flashService.error('Có lỗi xảy ra! Vui lòng thử lại sau!', true, true);
            this.router.navigate(['admin-portal/passbooks']);
        }

        this.validationMessages.value.invalidMinimumNumber = `Số tiền gửi tối thiểu là ${this.adminSetting.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}.`;
    }

    ngOnInit() {
        this.depositForm = this.fb.group({
            value: [this.depositViewModel.value, [
                Validators.required,
                numberValidator(),
                maximumNumberValidator(3000000000),
                minimumNumberValidator(10000)
            ]],
            trader: [this.depositViewModel.trader, [
                Validators.required,
                blankStringValidator()
            ]]
        });

        this.depositForm.valueChanges.subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    submitForm() {
        if (this.depositForm.invalid) {
            this.validateForm();
        } else {
            this.depositViewModel.value = this.depositForm.value.value;
            this.depositViewModel.trader = this.depositForm.value.trader;
            this.depositViewModel.savingAccount.employee = new Employee(new User({ uuid: localStorage.getItem('currentUser') }));
            this.savingAccountService.deposit(this.depositViewModel).subscribe(result => {
                switch (result.status) {
                    case 200:
                        this.flashService.success(result.message, true, true);
                        this.router.navigate([`admin-portal/passbooks/${this.depositViewModel.savingAccount.id}`]);
                        break;
                    default:
                        this.flashService.error(result.message);
                }
            });
        }
    }

    private onValueChanged(data?: any) {
        if (!this.depositForm) { return; }

        const form = this.depositForm;

        for (const field in this.formGroup) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && childControl.dirty && childControl.invalid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.dirty && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private validateForm() {
        if (!this.depositForm) { return; }
        const form = this.depositForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && !childControl.valid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private setErrorForControl(control: AbstractControl, field: string) {
        // clear previous error message (if any)
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
        }
    }
}