﻿import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AdminSettingService } from '../../admin-settings/admin-setting.service';

@Injectable()
export class MinimumDepositResolveService implements Resolve<any> {
    constructor(private adminSettingService: AdminSettingService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return this.adminSettingService.getMinimumDeposit();
    }
}