﻿import { SavingAccount } from '../saving-account';

export class DepositViewModel {
    savingAccount: SavingAccount;
    value: number;
    trader: string;

    constructor(depositViewModel: any = {}) {
        this.savingAccount = depositViewModel.savingAccount;
        this.value = depositViewModel.value;
        this.trader = depositViewModel.trader;
    }
}