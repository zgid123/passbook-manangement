﻿import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, Resolve, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';

import { SavingAccountService } from '../saving-account.service';
import { SavingAccountTypeService } from '../../saving-account-types/saving-account-type.service';
import { AdminSettingService } from '../../admin-settings/admin-setting.service';
import { FlashService } from '../../../shared/components/flash/flash.service';
import { DateFormatService } from '../../../shared/services/date-format.service';

import { SavingAccountType } from '../../saving-account-types/saving-account-type';
import { User } from '../../../shared/models/user';
import { AdminSetting } from '../../admin-settings/admin-setting';
import { SavingAccount } from '../saving-account';
import { Employee } from '../../employees/employee';
import { BankAccount } from '../../bank-accounts/bank-account';

import { dateValidator } from '../../../shared/directives/date.directive';
import { emailValidator } from '../../../shared/directives/email.directive';
import { personalIdentityValidator } from '../../../shared/directives/personal-identity.directive';
import { blankStringValidator } from '../../../shared/directives/blank-string.directive';
import { numberValidator } from '../../../shared/directives/number.directive';
import { minimumNumberValidator } from '../../../shared/directives/minimum-number.directive';
import { maximumNumberValidator } from '../../../shared/directives/maximum-number.directive';
import { phoneNumberValidator } from '../../../shared/directives/phone-number.directive';

@Component({
    moduleId: module.id,
    selector: 'content',
    templateUrl: '../shared/saving-account-form.component.html'
})

export class EditSavingAccountComponent implements OnInit, AfterViewInit {
    section = 'edit-passbook';
    title = 'Cập nhật tài khoản tiết kiệm';
    savingAccountTypes: SavingAccountType[] = [];
    adminSetting: AdminSetting = new AdminSetting({ name: '', value: 10000 });
    savingAccount: SavingAccount;

    savingAccountForm: FormGroup;

    private validationMessages = {
        'createdAt': {
            'required': 'Ngày mở là thông tin bắt buộc.',
            'invalidDate': 'Ngày mở không hợp lệ.'
        },
        'balance': {
            'required': 'Số tiền gửi là thông tin bắt buộc.',
            'invalidNumber': 'Số tiền gửi phải là số.',
            'invalidMinimumNumber': `Số tiền gửi tối thiểu là ${this.adminSetting.value}.`,
            'invalidMaximumNumber': 'Số tiền gửi tối đa là 3,000,000,000.'
        },
        'name': {
            'required': 'Tên khách hàng là thông tin bắt buộc.',
            'blankString': 'Tên khách hàng không được rỗng.'
        },
        'address': {
            'required': 'Địa chỉ là thông tin bắt buộc.',
            'blankString': 'Địa chỉ không được rỗng.'
        },
        'personalIdentity': {
            'required': 'Chứng minh nhân dân là thông tin bắt buộc.',
            'invalidPersonalIdentity': 'Chứng minh nhân dân không hợp lệ. Chỉ được nhập số (9 hoặc 12 chữ số).'
        },
        'email': {
            'required': 'Email là thông tin bắt buộc.',
            'blankString': 'Email không được rỗng.',
            'invalidEmail': 'Email không hợp lệ.'
        },
        'dateOfBirth': {
            'required': 'Ngày sinh là thông tin bắt buộc.',
            'invalidDate': 'Ngày sinh không hợp lệ.'
        },
        'phoneNumber': {
            'required': 'Số điện thoại là thông tin bắt buộc.',
            'blankString': 'Số điện thoại không được rỗng.',
            'invalidPhoneNumber': 'Số điện thoại không hợp lệ.\nMột số ví dụ hợp lệ:\n(123) 456-7890\n123-456 - 7890\n123.456.7890\n1234567890'
        }
    };

    private formErrors = {
        'createdAt': '',
        'balance': '',
        'name': '',
        'address': '',
        'personalIdentity': '',
        'email': '',
        'dateOfBirth': '',
        'phoneNumber': ''
    };

    private formGroup = {
        'createdAt': '',
        'bankAccount': '',
        'user': ''
    }

    constructor(private savingAccountService: SavingAccountService, private savingAccountTypeService: SavingAccountTypeService,
        private adminSettingService: AdminSettingService, private fb: FormBuilder, private router: Router, private route: ActivatedRoute,
        private flashService: FlashService, private dateFormatService: DateFormatService) {
        this.savingAccount = this.route.snapshot.data['savingAccount'];

        if (!this.savingAccount) {
            this.flashService.error('Tài khoản tiết kiệm không tồn tại!', true, true);
            this.router.navigate(['admin-portal/passbooks']);
        }

        this.savingAccountTypes = this.route.snapshot.data['savingAccountTypes'];
        this.savingAccount.savingAccountType = this.savingAccountTypes.filter(sat => sat.name == this.savingAccount.savingAccountType.name)[0];

        this.adminSetting = this.route.snapshot.data['adminSetting'];
        this.validationMessages.balance.invalidMinimumNumber = `Số tiền gửi tối thiểu là ${this.adminSetting.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}.`;
    }

    ngOnInit() {
        this.savingAccountForm = this.fb.group({
            createdAt: [this.savingAccount.createdAt, [
                Validators.required,
                dateValidator()
            ]],
            bankAccount: this.fb.group({
                balance: [this.savingAccount.bankAccount.balance, [
                    Validators.required,
                    numberValidator(),
                    minimumNumberValidator(this.adminSetting.value),
                    maximumNumberValidator(3000000000)
                ]]
            }),
            user: this.fb.group({
                name: [this.savingAccount.bankAccount.user.name, [
                    Validators.required,
                    blankStringValidator()
                ]],
                address: [this.savingAccount.bankAccount.user.address, [
                    Validators.required,
                    blankStringValidator()
                ]],
                personalIdentity: [this.savingAccount.bankAccount.user.personalIdentity, [
                    Validators.required,
                    personalIdentityValidator()
                ]],
                email: [this.savingAccount.bankAccount.user.email, [
                    Validators.required,
                    blankStringValidator(),
                    emailValidator()
                ]],
                dateOfBirth: [this.savingAccount.bankAccount.user.dateOfBirth, [
                    Validators.required,
                    dateValidator()
                ]],
                phoneNumber: [this.savingAccount.bankAccount.user.phoneNumber, [
                    Validators.required,
                    blankStringValidator(),
                    phoneNumberValidator()
                ]]
            }),
            savingAccountType: [this.savingAccount.savingAccountType]
        });

        this.savingAccountForm.valueChanges.subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    ngAfterViewInit() {
        let that = this;
        $('#createdAt').kendoDatePicker({
            value: this.dateFormatService.formatDate(this.savingAccount.createdAt),
            parseFormats: ['dd/MM/yyyy'],
            format: 'dd/MM/yyyy',
            change: function () {
                that.formErrors.createdAt = '';
                that.savingAccountForm.controls.createdAt.setValue(that.dateFormatService.formatDate(this.value()));
            }
        });
        $('#dateOfBirth').kendoDatePicker({
            value: this.dateFormatService.formatDate(this.savingAccount.bankAccount.user.dateOfBirth),
            parseFormats: ['dd/MM/yyyy'],
            format: 'dd/MM/yyyy',
            change: function () {
                that.formErrors.dateOfBirth = '';
                (<FormGroup>that.savingAccountForm.controls.user).controls.dateOfBirth.setValue(that.dateFormatService.formatDate(this.value()));
            }
        });
        $("#createdAt, #dateOfBirth").on("click", function () {
            $(this).data("kendoDatePicker").open();
        });
        $("#createdAt, #dateOfBirth").on("blur", function () {
            let date = $(this).data("kendoDatePicker").value();
            if (date && that.dateFormatService.formatDate(date).toString().includes('/') && !isNaN(Date.parse(date))) {
                that.formErrors[$(this).attr('id')] = '';
            }
        });
    }

    submitForm() {
        let createdAt = $('#createdAt').data('kendoDatePicker').value();
        let dateOfBirth = $('#dateOfBirth').data('kendoDatePicker').value();
        this.savingAccountForm.controls.createdAt.setValue(this.dateFormatService.formatDate(createdAt));
        (<FormGroup>this.savingAccountForm.controls.user).controls.dateOfBirth.setValue(this.dateFormatService.formatDate(dateOfBirth));

        if (this.savingAccountForm.invalid) {
            this.validateForm();
        } else {
            this.savingAccount.employee = new Employee(new User({ uuid: localStorage.getItem('currentUser') }));
            this.savingAccount.bankAccount = new BankAccount(this.savingAccountForm.value.bankAccount);
            this.savingAccount.bankAccount.user = new User(this.savingAccountForm.value.user);
            this.savingAccount.createdAt = createdAt;
            this.savingAccount.bankAccount.user.dateOfBirth = dateOfBirth;
            this.savingAccount.savingAccountType = this.savingAccountForm.value.savingAccountType;
            this.savingAccountService.updateSavingAccount(this.savingAccount).subscribe(result => {
                switch (result.status) {
                    case 200:
                        this.flashService.success(result.message, true, true);
                        this.router.navigate([`admin-portal/passbooks/${result.data.id}`]);
                        break;
                    default:
                        this.flashService.error(result.message);
                }
            });
        }
    }

    private onValueChanged(data?: any) {
        if (!this.savingAccountForm) { return; }

        if ($('#createdAt').data('kendoDatePicker')) {
            this.savingAccountForm.value.createdAt = $('#createdAt').data('kendoDatePicker').value();
        }

        if ($('#dateOfBirth').data('kendoDatePicker')) {
            this.savingAccountForm.value.user.dateOfBirth = $('#dateOfBirth').data('kendoDatePicker').value();
        }

        const form = this.savingAccountForm;

        for (const field in this.formGroup) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && childControl.dirty && childControl.invalid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.dirty && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private validateForm() {
        if (!this.savingAccountForm) { return; }
        const form = this.savingAccountForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && !childControl.valid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private setErrorForControl(control: AbstractControl, field: string) {
        // clear previous error message (if any)
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
        }
    }
}
