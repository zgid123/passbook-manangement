﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { EditSavingAccountComponent } from './saving-account-edit.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [EditSavingAccountComponent],
    bootstrap: [EditSavingAccountComponent]
})
export class EditSavingAccountModule { }
