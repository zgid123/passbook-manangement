﻿import { SavingAccountType } from '../saving-account-types/saving-account-type';
import { BankAccount } from '../bank-accounts/bank-account';
import { Employee } from '../employees/employee';

export class SavingAccount {
    index: number;
    id: number;
    createdAtString: string;
    active: boolean;
    bankAccount: BankAccount;
    closedAt: Date;
    createdAt: Date;
    employee: Employee;
    interestRate: number;
    term: number;
    reason: string;
    savingAccountType: SavingAccountType;
    status: number;
    interest: number;

    constructor(savingAccount: any = {}) {
        this.id = savingAccount.id;
        this.active = savingAccount.active;
        this.bankAccount = savingAccount.bankAccount;
        this.closedAt = savingAccount.closedAt;
        this.createdAt = savingAccount.createdAt;
        this.employee = savingAccount.employee;
        this.interestRate = savingAccount.interestRate;
        this.term = savingAccount.term;
        this.reason = savingAccount.reason;
        this.savingAccountType = savingAccount.savingAccountType;
        this.status = savingAccount.status;
        this.interest = savingAccount.interest;
    }
}