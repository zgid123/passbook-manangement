﻿import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { SavingAccount } from './saving-account';
import { SavingAccountSearchViewModel } from './saving-account-search-view-model';
import { DepositViewModel } from './deposit/deposit-view-model';
import { WithdrawViewModel } from './withdraw/withdraw-view-model';

@Injectable()
export class SavingAccountService {
    savingAccountViewModel: any;
    constructor(private http: Http, private router: Router) { }

    createSavingAccount(savingAccount: SavingAccount) {
        this.savingAccountViewModel = savingAccount;
        return this.http.post('/api/saving-accounts/create', this.savingAccountViewModel)
            .map((response: Response) => response.json());
    }

    updateSavingAccount(savingAccount: SavingAccount) {
        this.savingAccountViewModel = savingAccount;
        return this.http.put('/api/saving-accounts/update', this.savingAccountViewModel)
            .map((response: Response) => response.json());
    }

    getSavingAccounts(page: number = 1, savingAccountSearchViewModel: SavingAccountSearchViewModel = new SavingAccountSearchViewModel()) {
        return this.http.post(`/api/saving-accounts/get-all?page=${page}`, savingAccountSearchViewModel)
            .map((response: Response) => response.json());
    }

    getSavingAccount(id: number) {
        return this.http.get(`/api/saving-accounts/${id}`).map((response: Response) => response.json());
    }

    deposit(depositViewModel: DepositViewModel) {
        return this.http.put('/api/saving-accounts/deposit', depositViewModel)
            .map((response: Response) => response.json());
    }

    withdraw(withdrawViewModel: WithdrawViewModel) {
        return this.http.put('/api/saving-accounts/withdraw', withdrawViewModel)
            .map((response: Response) => response.json());
    }
}
