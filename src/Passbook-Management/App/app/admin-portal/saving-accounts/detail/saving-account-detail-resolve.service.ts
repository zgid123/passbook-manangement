﻿import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SavingAccountService } from '../saving-account.service';

@Injectable()
export class SavingAccountDetailResolveService implements Resolve<any> {
    constructor(private savingAccountService: SavingAccountService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return this.savingAccountService.getSavingAccount(route.params['id']);
    }
}