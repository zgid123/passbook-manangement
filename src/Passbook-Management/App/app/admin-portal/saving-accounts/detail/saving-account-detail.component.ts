﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { FlashService } from '../../../shared/components/flash/flash.service';

import { SavingAccount } from '../saving-account';

@Component({
    selector: 'content',
    moduleId: module.id,
    templateUrl: './saving-account-detail.component.html'
})
export class SavingAccountDetailComponent {
    savingAccount: SavingAccount;

    constructor(private router: Router, private route: ActivatedRoute, private flashService: FlashService) {
        this.savingAccount = this.route.snapshot.data['savingAccount'];

        if (!this.savingAccount) {
            this.flashService.error('Tài khoản tiết kiệm không tồn tại.', true, true);
            this.router.navigate(['/admin-portal/saving-accounts']);
        }
    }
}
