﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SavingAccountDetailComponent } from './saving-account-detail.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [SavingAccountDetailComponent],
    bootstrap: [SavingAccountDetailComponent]
})
export class SavingAccountDetailModule { }