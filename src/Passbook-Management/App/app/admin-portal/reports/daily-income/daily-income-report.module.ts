﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DailyIncomeReportComponent } from './daily-income-report.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [DailyIncomeReportComponent],
    bootstrap: [DailyIncomeReportComponent]
})
export class DailyIncomeReportModule { }