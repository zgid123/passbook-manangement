﻿export class DailyIncomeReportViewModel {
    index: number;
    savingAccountType: string;
    income: number;
    disbursement: number;
    total: number;

    constructor(dailyIncomeReportViewModel: any = {}) {
        this.savingAccountType = dailyIncomeReportViewModel.savingAccountType;
        this.income = dailyIncomeReportViewModel.income;
        this.disbursement = dailyIncomeReportViewModel.disbursement;
        this.total = dailyIncomeReportViewModel.total;
    }
}