﻿import { Component, AfterViewInit } from '@angular/core';

import { DailyIncomeReportViewModel } from './daily-income-report-view-model';

import { ReportService } from '../report.service';
import { DateFormatService } from '../../../shared/services/date-format.service';

@Component({
    moduleId: module.id,
    selector: 'content',
    templateUrl: './daily-income-report.component.html'
})

export class DailyIncomeReportComponent implements AfterViewInit {
    date: any;
    dailyIncomeReportViewModels: DailyIncomeReportViewModel[] = [];
    constructor(private reportService: ReportService, private dateFormatService: DateFormatService) { }

    ngAfterViewInit() {
        let that = this;

        let $grid = $('#report');

        $grid.kendoGrid({
            dataSource: {
                data: this.dailyIncomeReportViewModels
            },
            toolbar: ["pdf"],
            sortable: {
                mode: 'multiple',
                allowUnsort: true
            },
            scrollable: true,
            groupable: true,
            height: '496px',
            columns: [
                { field: 'index', title: 'STT', width: 60 },
                { field: 'savingAccountType', title: 'Loại tài khoản', minResizableWidth: 140 },
                { field: 'income', title: 'Tổng thu', format: '{0:n0} VNĐ', minResizableWidth: 140 },
                { field: 'disbursement', title: 'Tổng chi', format: '{0:n0} VNĐ', minResizableWidth: 140 },
                { field: 'total', title: 'Chênh lệch', format: '{0:n0} VNĐ', minResizableWidth: 140 }
            ]
        }).data('kendoGrid');

        let datepicker = $('#report-date').kendoDatePicker({
            parseFormats: ['dd/MM/yyyy'],
            format: 'dd/MM/yyyy'
        });

        datepicker.data("kendoDatePicker").bind('change', function () {
            that.date = this.value();
            that.reportService.getDailyIncomeReport(that.date).subscribe(result => {
                that.dailyIncomeReportViewModels = result;

                for (const dailyIncomeReportViewModel in that.dailyIncomeReportViewModels) {
                    that.dailyIncomeReportViewModels[dailyIncomeReportViewModel].index = parseInt(dailyIncomeReportViewModel) + 1;
                    that.dailyIncomeReportViewModels[dailyIncomeReportViewModel].total = that.dailyIncomeReportViewModels[dailyIncomeReportViewModel].income - that.dailyIncomeReportViewModels[dailyIncomeReportViewModel].disbursement;
                }

                $grid.data('kendoGrid').setOptions({
                    pdf: {
                        allPages: true,
                        avoidLinks: true,
                        fileName: 'Baocao_' + that.dateFormatService.formatDate(that.date).replace('/', '-').trim() + '.pdf',
                        paperSize: "A4",
                        proxyURL: "/proxy",
                        margin: { top: "3cm", left: "0.5cm", right: "0.5cm", bottom: "1cm" },
                        template: $(".daily-income-report-template").html()
                    }
                });
                $grid.data('kendoGrid').dataSource.data(that.dailyIncomeReportViewModels);
            });
        });
    }
}