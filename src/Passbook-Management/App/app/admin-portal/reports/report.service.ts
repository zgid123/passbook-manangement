﻿import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { DailyIncomeReportViewModel } from './daily-income/daily-income-report-view-model';

@Injectable()
export class ReportService {
    constructor(private http: Http, private router: Router) { }

    getDailyIncomeReport(date: Date) {
        return this.http.post('/api/reports/daily-income', date)
            .map((response: Response) => response.json());
    }

    getMonthlySavingAccountsReport(date: Date, savingAccountType: string) {
        return this.http.post('/api/reports/monthly-saving-accounts', { date: date, savingAccountType: savingAccountType })
            .map((response: Response) => response.json());
    }
}
