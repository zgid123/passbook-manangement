﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MonthlySavingAccountsReportComponent } from './monthly-saving-accounts-report.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [MonthlySavingAccountsReportComponent],
    bootstrap: [MonthlySavingAccountsReportComponent]
})
export class MonthlySavingAccountsReportModule { }