﻿import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';

import { MonthlySavingAccountsReportViewModel } from './monthly-saving-accounts-report-view-model';
import { SavingAccountType } from '../../saving-account-types/saving-account-type';

import { ReportService } from '../report.service';
import { DateFormatService } from '../../../shared/services/date-format.service';

@Component({
    moduleId: module.id,
    selector: 'content',
    templateUrl: './monthly-saving-accounts-report.component.html'
})

export class MonthlySavingAccountsReportComponent implements OnInit, AfterViewInit {
    date: any;
    savingAccountType: SavingAccountType;
    savingAccountTypes: SavingAccountType[] = [];
    monthlySavingAccountsReportViewModels: MonthlySavingAccountsReportViewModel[] = [];

    monthlyReportForm: FormGroup;

    private validationMessages = {
        'date': {
            'required': 'Vui lòng chọn ngày báo cáo.',
        }
    };

    private formErrors = {
        'date': ''
    };

    private formGroup = {
        'date': '',
    }

    constructor(private reportService: ReportService, private dateFormatService: DateFormatService,
        private router: Router, private route: ActivatedRoute, private fb: FormBuilder) {
        this.savingAccountTypes = this.route.snapshot.data['savingAccountTypes'];

        this.savingAccountType = this.savingAccountTypes[0];
    }

    ngOnInit() {
        this.monthlyReportForm = this.fb.group({
            date: ['', [
                Validators.required
            ]],
            savingAccountType: [this.savingAccountType]
        });
    }

    ngAfterViewInit() {
        let that = this;

        let $grid = $('#report');

        $grid.kendoGrid({
            dataSource: {
                data: this.monthlySavingAccountsReportViewModels
            },
            toolbar: ["pdf"],
            sortable: {
                mode: 'multiple',
                allowUnsort: true
            },
            scrollable: true,
            groupable: true,
            height: '496px',
            columns: [
                { field: 'index', title: 'STT', width: 60 },
                { field: 'dateString', title: 'Ngày', minResizableWidth: 140 },
                { field: 'creations', title: 'Tổng mở', format: '{0:n0}', minResizableWidth: 140 },
                { field: 'closures', title: 'Tổng đóng', format: '{0:n0}', minResizableWidth: 140 },
                { field: 'total', title: 'Chênh lệch', format: '{0:n0}', minResizableWidth: 140 }
            ]
        }).data('kendoGrid');

        let datepicker = $('#date').kendoDatePicker({
            parseFormats: ['MM/yyyy'],
            start: "year",
            depth: "year",
            format: "MM/yyyy",
            dateInput: true
        });
    }

    submitForm() {
        let that = this;
        let date = $('#date').data('kendoDatePicker').value();
        this.date = date;
        this.monthlyReportForm.controls.date.setValue(this.dateFormatService.formatMonthYear(date));

        this.validateForm();

        if (this.monthlyReportForm.valid) {
            this.savingAccountType = this.monthlyReportForm.value.savingAccountType;
            this.reportService.getMonthlySavingAccountsReport(this.date, this.savingAccountType.name).subscribe(result => {
                that.monthlySavingAccountsReportViewModels = result;

                if (that.monthlySavingAccountsReportViewModels.length == 1 && that.monthlySavingAccountsReportViewModels[0].date.toString().includes('0001-01')) {
                    that.monthlySavingAccountsReportViewModels.pop();
                    return;
                }

                for (const monthlySavingAccountsReportViewModel in that.monthlySavingAccountsReportViewModels) {
                    that.monthlySavingAccountsReportViewModels[monthlySavingAccountsReportViewModel].index = parseInt(monthlySavingAccountsReportViewModel) + 1;
                    that.monthlySavingAccountsReportViewModels[monthlySavingAccountsReportViewModel].dateString = that.dateFormatService.formatDate(that.monthlySavingAccountsReportViewModels[monthlySavingAccountsReportViewModel].date);
                    that.monthlySavingAccountsReportViewModels[monthlySavingAccountsReportViewModel].total = that.monthlySavingAccountsReportViewModels[monthlySavingAccountsReportViewModel].creations - that.monthlySavingAccountsReportViewModels[monthlySavingAccountsReportViewModel].closures;
                }

                let $grid = $('#report').data('kendoGrid');
                $grid.setOptions({
                    pdf: {
                        allPages: true,
                        avoidLinks: true,
                        fileName: 'Baocao_' + that.dateFormatService.formatDate(that.date).replace('/', '-').trim() + '.pdf',
                        paperSize: "A4",
                        proxyURL: "/proxy",
                        margin: { top: "3cm", left: "0.5cm", right: "0.5cm", bottom: "1cm" },
                        template: $(".monthly-saving-accounts-report-template").html()
                    }
                });

                $grid.dataSource.data(that.monthlySavingAccountsReportViewModels);
            });
        }
    }

    private validateForm() {
        if (!this.monthlyReportForm) { return; }
        const form = this.monthlyReportForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && !childControl.valid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private setErrorForControl(control: AbstractControl, field: string) {
        // clear previous error message (if any)
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
        }
    }
}
