﻿export class MonthlySavingAccountsReportViewModel {
    index: number;
    date: Date;
    dateString: string;
    creations: number;
    closures: number;
    total: number;

    constructor(dailyIncomeReportViewModel: any = {}) {
        this.date = dailyIncomeReportViewModel.date;
        this.creations = dailyIncomeReportViewModel.creations;
        this.closures = dailyIncomeReportViewModel.closures;
        this.total = dailyIncomeReportViewModel.total;
    }
}