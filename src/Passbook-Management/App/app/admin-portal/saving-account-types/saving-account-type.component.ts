﻿import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';

import { SavingAccountType } from './saving-account-type';

@Component({
    selector: 'content',
    moduleId: module.id,
    templateUrl: './saving-account-type.component.html'
})
export class SavingAccountTypeComponent implements OnInit, AfterViewInit {
    savingAccountTypes: SavingAccountType[] = [];

    constructor( @Inject(DOCUMENT) private document: Document, private router: Router, private route: ActivatedRoute) {
        this.savingAccountTypes = this.route.snapshot.data['savingAccountTypes'];
        if (this.savingAccountTypes) {
            for (const savingAccountType in this.savingAccountTypes) {
                this.savingAccountTypes[savingAccountType].index = parseInt(savingAccountType) + 1;
            }
        }
    }

    ngOnInit() { }

    ngAfterViewInit() {
        let that = this;
        let $grid = $('#saving-account-type-list');

        $grid.kendoGrid({
            dataSource: {
                data: this.savingAccountTypes
            },
            sortable: {
                mode: 'multiple',
                allowUnsort: true
            },
            scrollable: true,
            groupable: true,
            height: '496px',
            columns: [
                { field: 'index', title: 'STT', width: 60 },
                { field: 'name', title: 'Tên loại', minResizableWidth: 140 },
                { field: 'interestRate', title: 'Lãi suất', minResizableWidth: 140 },
                { field: 'term', title: 'Lãi suất theo kỳ hạn', minResizableWidth: 140 },
                {
                    command:
                    {
                        text: ' ',
                        template: '<a class="k-button k-button-icontext k-grid- edit-detail"><span class="fa fa-pencil"></span></a>'
                    },
                    title: ' ',
                    width: 45
                }
            ]
        }).data('kendoGrid');

        $grid.kendoTooltip({
            filter: ".k-button.edit-detail",
            content: 'Chỉnh sửa'
        });

        $('.k-button.k-button-icontext.edit-detail').on('click', function (event: any) {
            let $grid = $('#saving-account-type-list').data('kendoGrid');
            var dataItem = $grid.dataItem($(this).closest("tr"));
            that.router.navigate([`admin-portal/passbook-types/edit/${dataItem.id}`]);
        });
    }
}