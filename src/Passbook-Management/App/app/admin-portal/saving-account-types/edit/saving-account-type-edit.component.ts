﻿import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, Resolve, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';

import { SavingAccountTypeService } from '../../saving-account-types/saving-account-type.service';
import { FlashService } from '../../../shared/components/flash/flash.service';

import { SavingAccountType } from '../../saving-account-types/saving-account-type';

import { floatNumberValidator } from '../../../shared/directives/float-number.directive';
import { numberValidator } from '../../../shared/directives/number.directive';

@Component({
    moduleId: module.id,
    selector: 'content',
    templateUrl: '../shared/saving-account-type-form.component.html'
})

export class EditSavingAccountTypeComponent implements OnInit, AfterViewInit {
    section = 'edit-passbook-type';
    title = 'Cập nhật loại tài khoản tiết kiệm';
    isNoTerm: boolean = false;
    savingAccountType: SavingAccountType;

    savingAccountTypeForm: FormGroup;

    private validationMessages = {
        'name': {
            'required': 'Tên loại tài khoản là thông tin bắt buộc.',
            'invalidNumber': 'Tên loại tài khoản phải là số nguyên không dấu.'
        },
        'interestRate': {
            'required': 'Lãi suất là thông tin bắt buộc.',
            'invalidFloatNumber': 'Lãi suất chỉ được là số hoặc số thập phân.'
        },
        'term': {
            'required': 'Lãi suất theo kỳ hạn là thông tin bắt buộc.',
            'invalidFloatNumber': 'Lãi suất theo kỳ hạn chỉ được là số hoặc số thập phân.'
        }
    };

    private formErrors = {
        'interestRate': '',
        'term': '',
        'name': ''
    };

    private formGroup = {
        'interestRate': '',
        'term': '',
        'name': ''
    }

    constructor(private savingAccountTypeService: SavingAccountTypeService, private fb: FormBuilder, private router: Router,
        private route: ActivatedRoute, private flashService: FlashService) {
        this.savingAccountType = this.route.snapshot.data['savingAccountType'];

        if (!this.savingAccountType) {
            this.flashService.error('Loại tài khoản tiết kiệm không tồn tại!', true, true);
            this.router.navigate(['admin-portal/passbook-types']);
        }

        if (this.savingAccountType.name == 'Không kỳ hạn') {
            this.isNoTerm = true;
        }

        this.savingAccountType.name.replace(' tháng', '');
    }

    ngOnInit() {
        this.savingAccountTypeForm = this.fb.group({
            name: [parseInt(this.savingAccountType.name, 10), [
                Validators.required,
                numberValidator()
            ]],
            interestRate: [this.savingAccountType.interestRate, [
                Validators.required,
                floatNumberValidator()
            ]],
            term: [this.savingAccountType.term, [
                Validators.required,
                floatNumberValidator()
            ]]
        });

        this.savingAccountTypeForm.valueChanges.subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    ngAfterViewInit() { }

    submitForm() {
        if (this.savingAccountType.name == 'Không kỳ hạn') {
            this.savingAccountTypeForm.value.name = 'Không kỳ hạn';
            this.savingAccountTypeForm.value.term = 0.0
        }
        if (this.savingAccountTypeForm.invalid) {
            this.validateForm();
        } else {
            if (this.savingAccountType.name != 'Không kỳ hạn') {
                this.savingAccountType.name = this.savingAccountTypeForm.value.name;
                this.savingAccountType.term = this.savingAccountTypeForm.value.term;
            }
            this.savingAccountType.interestRate = this.savingAccountTypeForm.value.interestRate;
            this.savingAccountTypeService.updateSavingAccountType(this.savingAccountType).subscribe(result => {
                switch (result.status) {
                    case 200:
                        this.flashService.success(result.message, true, true);
                        this.router.navigate(['admin-portal/passbook-types']);
                        break;
                    default:
                        this.flashService.error(result.message);
                }
            });
        }
    }

    private onValueChanged(data?: any) {
        if (!this.savingAccountTypeForm) { return; }

        const form = this.savingAccountTypeForm;

        for (const field in this.formGroup) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && childControl.dirty && childControl.invalid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.dirty && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private validateForm() {
        if (!this.savingAccountTypeForm) { return; }
        const form = this.savingAccountTypeForm;

        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control instanceof FormGroup) {
                for (const subField in (<FormGroup>control).controls) {
                    this.formErrors[subField] = '';
                    const childControl = control.get(subField);

                    if (childControl && !childControl.valid) {
                        this.setErrorForControl(childControl, subField);
                    }
                }
            } else if (control && control.invalid) {
                this.setErrorForControl(control, field);
            }
        }
    }

    private setErrorForControl(control: AbstractControl, field: string) {
        // clear previous error message (if any)
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
            this.formErrors[field] += messages[key] + ' ';
        }
    }
}
