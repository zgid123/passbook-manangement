﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { EditSavingAccountTypeComponent } from './saving-account-type-edit.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [EditSavingAccountTypeComponent],
    bootstrap: [EditSavingAccountTypeComponent]
})
export class EditSavingAccountTypeModule { }