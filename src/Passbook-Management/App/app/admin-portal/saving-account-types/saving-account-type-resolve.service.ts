﻿import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SavingAccountTypeService } from './saving-account-type.service';

@Injectable()
export class SavingAccountTypeResolveService implements Resolve<any> {
    constructor(private savingAccountTypeService: SavingAccountTypeService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return this.savingAccountTypeService.getSavingAccountTypes();
    }
}