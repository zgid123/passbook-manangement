﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SavingAccountTypeComponent } from './saving-account-type.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [SavingAccountTypeComponent],
    bootstrap: [SavingAccountTypeComponent]
})
export class SavingAccountTypeModule { }