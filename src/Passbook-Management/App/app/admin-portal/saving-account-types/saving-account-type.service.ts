﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { SavingAccountType } from './saving-account-type';

@Injectable()
export class SavingAccountTypeService {
    constructor(private http: Http, private router: Router) { }

    getSavingAccountTypes() {
        return this.http.get('/api/saving-account-types')
            .map((response: Response) => response.json());
    }

    getSavingAccountType(id: number) {
        return this.http.get(`/api/saving-account-types/${id}`)
            .map((response: Response) => response.json());
    }

    updateSavingAccountType(savingAccountTypeViewModel: SavingAccountType) {
        return this.http.put('/api/saving-account-types/update', savingAccountTypeViewModel)
            .map((response: Response) => response.json());
    }

    createSavingAccountType(savingAccountTypeViewModel: SavingAccountType) {
        return this.http.post('/api/saving-account-types/create', savingAccountTypeViewModel)
            .map((response: Response) => response.json());
    }
}
