﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NewSavingAccountTypeComponent } from './saving-account-type-new.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [NewSavingAccountTypeComponent],
    bootstrap: [NewSavingAccountTypeComponent]
})
export class NewSavingAccountTypeModule { }