﻿export class SavingAccountType {
    index: number;
    id: number
    interestRate: number;
    term: number;
    name: string;

    constructor(savingAccountType: any = {}) {
        this.id = savingAccountType.id;
        this.interestRate = savingAccountType.interestRate;
        this.term = savingAccountType.term;
        this.name = savingAccountType.name;
    }
}