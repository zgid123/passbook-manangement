﻿import { Injectable } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class FlashService {
    private subject = new BehaviorSubject<any>(null);
    private keepAfterNavigationChange = false;
    private show = new BehaviorSubject<any>(false);

    constructor(private router: Router) {
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    this.keepAfterNavigationChange = false;
                } else {
                    this.subject.next(null);
                }
            } else if (event instanceof NavigationEnd) {
                if (this.subject.value) {
                    this.show.next(true);
                }
            }
        });
    }

    success(message: string, keepAfterNavigationChange = false, skipNow = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
        this.show.next(!skipNow);
    }

    error(message: string, keepAfterNavigationChange = false, skipNow = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
        this.show.next(!skipNow);
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }

    showMessage(): Observable<any> {
        return this.show.asObservable();
    }
}
