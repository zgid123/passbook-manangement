﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FlashComponent } from './flash.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [FlashComponent],
    bootstrap: [FlashComponent]
})
export class FlashModule { }