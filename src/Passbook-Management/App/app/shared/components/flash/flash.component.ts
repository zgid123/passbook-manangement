﻿import { Component, OnInit } from '@angular/core';
import { FlashService } from './flash.service';

@Component({
    moduleId: module.id,
    selector: 'flash',
    templateUrl: './flash.component.html'
})

export class FlashComponent implements OnInit {
    message: any = { type: '', text: '' };
    alertType: string = '';
    isHide: boolean = true;
    timeOut: any;

    constructor(public flashService: FlashService) { }

    ngOnInit() {
        this.flashService.getMessage().subscribe((message) => {
            if (message) {
                this.message = message;
                this.alertType = 'alert-' + message.type;
            }
        });

        this.flashService.showMessage().subscribe((isShown) => {
            this.isHide = !isShown;
            if (this.timeOut) {
                clearTimeout(this.timeOut);
            }
            this.timeOut = setTimeout(() => this.isHide = true, 3000);
        });
    }

    hideAlert() {
        this.message = { type: '', text: '' };
        this.isHide = true;
    }
}