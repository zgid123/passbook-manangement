﻿import { Injectable } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class PreLoaderService {
    private show = new BehaviorSubject<any>(false);

    constructor(private router: Router) {
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                this.show.next(true);
            } else if (event instanceof NavigationEnd) {
                this.show.next(false);
            }
        });
    }

    hide(isHide: boolean = true) {
        this.show.next(!isHide);
    }

    showLoader(): Observable<any> {
        return this.show.asObservable();
    }
}
