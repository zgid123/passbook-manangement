﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PreLoaderComponent } from './pre-loader.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [PreLoaderComponent],
    bootstrap: [PreLoaderComponent]
})
export class PreLoaderModule { }