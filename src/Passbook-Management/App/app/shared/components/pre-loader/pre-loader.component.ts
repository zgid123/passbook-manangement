﻿import { Component, OnInit } from '@angular/core';

import { PreLoaderService } from './pre-loader.service';

@Component({
    moduleId: module.id,
    selector: 'loader',
    templateUrl: './pre-loader.component.html'
})

export class PreLoaderComponent implements OnInit {
    isHide: boolean = true;

    constructor(public preLoaderService: PreLoaderService) { }

    ngOnInit() {
        this.preLoaderService.showLoader().subscribe((isShown) => {
            this.isHide = !isShown;
        });
    }
}