﻿import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

export function phoneNumberValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let phoneNumberRegex = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/
        if (control.value && !phoneNumberRegex.test(control.value)) {
            return { invalidPhoneNumber: true };
        }
        return Validators.nullValidator;
    };
}

@Directive({
    selector: '[phoneNumberValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: PhoneNumberDirective, multi: true }]
})
export class PhoneNumberDirective implements Validator, OnChanges {
    @Input() blankString: string;
    private valFn = Validators.nullValidator;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['phoneNumberValidator'];
        if (change) {
            this.valFn = phoneNumberValidator();
        } else {
            this.valFn = Validators.nullValidator;
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}
