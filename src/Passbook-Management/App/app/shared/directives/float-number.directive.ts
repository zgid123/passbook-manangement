﻿import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

export function floatNumberValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (control.value && !/([0-9]+[.])?[0-9]+/g.test(control.value)) {
            return { invalidFloatNumber: true };
        }
        return Validators.nullValidator;
    };
}

@Directive({
    selector: '[floatNumberValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: FloatNumberDirective, multi: true }]
})
export class FloatNumberDirective implements Validator, OnChanges {
    @Input() blankString: string;
    private valFn = Validators.nullValidator;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['floatNumberValidator'];
        if (change) {
            this.valFn = floatNumberValidator();
        } else {
            this.valFn = Validators.nullValidator;
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}