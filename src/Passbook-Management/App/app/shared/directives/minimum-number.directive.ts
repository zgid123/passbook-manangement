﻿import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

export function minimumNumberValidator(number: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (!isNaN(parseInt(control.value, 10)) && control.value < number) {
            return { invalidMinimumNumber: true };
        }
        return Validators.nullValidator;
    };
}

@Directive({
    selector: '[minimumNumberValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: MinimumNumberDirective, multi: true }]
})
export class MinimumNumberDirective implements Validator, OnChanges {
    @Input() number: number;
    private valFn = Validators.nullValidator;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['minimumNumberValidator'];
        if (change) {
            this.valFn = minimumNumberValidator(this.number);
        } else {
            this.valFn = Validators.nullValidator;
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}