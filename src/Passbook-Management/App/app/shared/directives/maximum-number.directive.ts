﻿import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

export function maximumNumberValidator(number: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (!isNaN(parseInt(control.value, 10)) && control.value > number) {
            return { invalidMaximumNumber: true };
        }
        return Validators.nullValidator;
    };
}

@Directive({
    selector: '[maximumNumberValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: MaximumNumberDirective, multi: true }]
})
export class MaximumNumberDirective implements Validator, OnChanges {
    @Input() number: number;
    private valFn = Validators.nullValidator;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['maximumNumberValidator'];
        if (change) {
            this.valFn = maximumNumberValidator(this.number);
        } else {
            this.valFn = Validators.nullValidator;
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}