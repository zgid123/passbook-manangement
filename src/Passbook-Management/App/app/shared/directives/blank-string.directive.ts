﻿import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

export function blankStringValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (control.value && /^\s*$/.test(control.value)) {
            return { blankString: true };
        }
        return Validators.nullValidator;
    };
}

@Directive({
    selector: '[blankStringValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: BlankStringDirective, multi: true }]
})
export class BlankStringDirective implements Validator, OnChanges {
    @Input() blankString: string;
    private valFn = Validators.nullValidator;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['blankStringValidator'];
        if (change) {
            this.valFn = blankStringValidator();
        } else {
            this.valFn = Validators.nullValidator;
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}