﻿import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

export function dateValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (!control.value || !control.value.toString().includes('/') || (isNaN(Date.parse(control.value)) && isNaN(Date.parse(control.value.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"))))) {
            return { invalidDate: true };
        }
        return Validators.nullValidator;
    };
}

@Directive({
    selector: '[dateValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: DateDirective, multi: true }]
})
export class DateDirective implements Validator, OnChanges {
    @Input() date: Date;
    private valFn = Validators.nullValidator;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['dateValidator'];
        if (change) {
            this.valFn = dateValidator();
        } else {
            this.valFn = Validators.nullValidator;
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}