﻿import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

export function numberValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (isNaN(control.value)) {
            return { invalidNumber: true };
        }
        return Validators.nullValidator;
    };
}

@Directive({
    selector: '[numberValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: NumberDirective, multi: true }]
})
export class NumberDirective implements Validator, OnChanges {
    @Input() number: number;
    private valFn = Validators.nullValidator;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['numberValidator'];
        if (change) {
            this.valFn = numberValidator();
        } else {
            this.valFn = Validators.nullValidator;
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}