﻿import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

export function personalIdentityValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let identityRegexp = /^(\d{9}|\d{12})?$/;
        if (control.value && !identityRegexp.test(control.value)) {
            return { invalidPersonalIdentity: true };
        }
        return Validators.nullValidator;
    };
}

@Directive({
    selector: '[personalIdentityValidator]',
    providers: [{ provide: NG_VALIDATORS, useExisting: PersonalIdentityDirective, multi: true }]
})
export class PersonalIdentityDirective implements Validator, OnChanges {
    @Input() personalIdentity: string;
    private valFn = Validators.nullValidator;

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['personalIdentityValidator'];
        if (change) {
            this.valFn = personalIdentityValidator();
        } else {
            this.valFn = Validators.nullValidator;
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}