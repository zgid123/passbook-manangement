﻿export class DateFormatService {
    public formatDate(value: Date, format: string = 'dd/MM/yyyy') {
        if (value) {
            try {
                var timestamp = Number(value);
                var date;
                if (isNaN(timestamp)) {
                    date = new Date(value);
                }
                else {
                    date = new Date(timestamp);
                }
                var y = date.getFullYear();
                var M = (Number(date.getMonth() + 1) < 10) ? "0" + Number(date.getMonth() + 1) : Number(date.getMonth() + 1);
                var d = (date.getDate() < 10) ? "0" + date.getDate() : date.getDate();
                var dateDelimiter = "/";

                if (format == 'dd/MM/yyyy') {
                    return d + dateDelimiter + M + dateDelimiter + y;
                } else {
                    return M + dateDelimiter + d + dateDelimiter + y;
                }
            }
            catch (error) {

            }
        }
    }

    public formatMonthYear(value: Date) {
        if (value) {
            try {
                var timestamp = Number(value);
                var date;
                if (isNaN(timestamp)) {
                    date = new Date(value);
                }
                else {
                    date = new Date(timestamp);
                }
                var y = date.getFullYear();
                var M = (Number(date.getMonth() + 1) < 10) ? "0" + Number(date.getMonth() + 1) : Number(date.getMonth() + 1);
                var d = (date.getDate() < 10) ? "0" + date.getDate() : date.getDate();
                var dateDelimiter = "/";

                return M + dateDelimiter + y;
            }
            catch (error) {

            }
        }
    }
}
