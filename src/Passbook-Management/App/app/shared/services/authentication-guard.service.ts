﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { FlashService } from '../components/flash/flash.service';
import { AuthenticationService } from './authentication.service';

import { User } from '../models/user';

@Injectable()
export class AuthenticationGuard implements CanActivate {
    user: User;

    constructor(private router: Router, private flashService: FlashService, private authenticationService: AuthenticationService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.authenticationService.getInformation().map(user => {
            if (user && user.role == 'Admin') {
                return true;
            }

            this.flashService.error('Bạn cần đăng nhập trước khi tiếp tục.', true, true);
            this.router.navigate(['/']);
            return false;
        });
    }
}