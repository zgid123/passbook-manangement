﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { LoginViewModel } from '../../shared/models/login-view-model';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {
    constructor(private http: Http, private router: Router) { }

    login(email: string, password: string, rememberMe: boolean) {
        return this.http.post('/api/account/login', new LoginViewModel(email, password, rememberMe))
            .map((response: Response) => {
                let result = response.json();
                if (result && result.authToken) {
                    localStorage.setItem('currentUser', result.authToken);
                }
                return result;
            });
    }

    getInformation() {
        return this.http.get(`/api/account/${localStorage.getItem('currentUser')}`)
            .map((response: Response) => response.json());
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.router.navigate(['/']);
    }
}