﻿export class User {
    uuid: string;
    email: string;
    phoneNumber: string;
    name: string;
    personalIdentity: string;
    address: string;
    dateOfBirth: Date;
    createdAt: Date;
    role: string;

    constructor(user: any) {
        this.uuid = user.uuid;
        this.email = user.email;
        this.phoneNumber = user.phoneNumber;
        this.name = user.name;
        this.personalIdentity = user.personalIdentity;
        this.address = user.address;
        this.dateOfBirth = user.dateOfBirth || new Date();
        this.createdAt = user.createdAt;
        this.role = user.role;
    }
}