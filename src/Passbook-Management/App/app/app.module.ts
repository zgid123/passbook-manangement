﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './landing-page/navbar/login/login.component';
import { FlashComponent } from './shared/components/flash/flash.component';
import { PreLoaderComponent } from './shared/components/pre-loader/pre-loader.component';

import { AuthenticationService } from './shared/services/authentication.service';
import { AuthenticationGuard } from './shared/services/authentication-guard.service';
import { AuthenticationResolveService } from './shared/services/authentication-resolve.service';
import { FlashService } from './shared/components/flash/flash.service';
import { PreLoaderService } from './shared/components/pre-loader/pre-loader.service';
import { DateFormatService } from './shared/services/date-format.service';

import { LandingPageComponent } from './landing-page/landing-page.component';
import { NavbarComponent } from './landing-page/navbar/navbar.component';

import { AdminPortalComponent } from './admin-portal/admin-portal.component';
import { OverviewComponent } from './admin-portal/overview/overview.component';
import { SavingAccountComponent } from './admin-portal/saving-accounts/saving-account.component';
import { NewSavingAccountComponent } from './admin-portal/saving-accounts/create/saving-account-new.component';
import { SavingAccountDetailComponent } from './admin-portal/saving-accounts/detail/saving-account-detail.component';
import { EditSavingAccountComponent } from './admin-portal/saving-accounts/edit/saving-account-edit.component';
import { DepositSavingAccountComponent } from './admin-portal/saving-accounts/deposit/saving-account-deposit.component';
import { WithdrawSavingAccountComponent } from './admin-portal/saving-accounts/withdraw/saving-account-withdraw.component';
import { SavingAccountHistoryComponent } from './admin-portal/saving-accounts/history/saving-account-history.component';
import { SavingAccountTypeComponent } from './admin-portal/saving-account-types/saving-account-type.component';
import { NewSavingAccountTypeComponent } from './admin-portal/saving-account-types/create/saving-account-type-new.component';
import { EditSavingAccountTypeComponent } from './admin-portal/saving-account-types/edit/saving-account-type-edit.component';
import { AdminSettingComponent } from './admin-portal/admin-settings/admin-setting.component';
import { EditAdminSettingComponent } from './admin-portal/admin-settings/edit/admin-setting-edit.component';
import { TransactionHistoryComponent } from './admin-portal/transaction-histories/transaction-history.component';
import { DailyIncomeReportComponent } from './admin-portal/reports/daily-income/daily-income-report.component';
import { MonthlySavingAccountsReportComponent } from './admin-portal/reports/monthly-saving-accounts/monthly-saving-accounts-report.component';

import { SavingAccountService } from './admin-portal/saving-accounts/saving-account.service';
import { SavingAccountResolveService } from './admin-portal/saving-accounts/saving-account-resolve.service';
import { SavingAccountDetailResolveService } from './admin-portal/saving-accounts/detail/saving-account-detail-resolve.service';
import { EditSavingAccountResolveService } from './admin-portal/saving-accounts/edit/saving-account-edit-resolve.service';
import { MinimumInitialResolveService } from './admin-portal/saving-accounts/shared/minimum-initial-resolve.service';
import { MinimumDepositResolveService } from './admin-portal/saving-accounts/deposit/minimum-deposit-resolve.service';
import { SavingAccountTransactionHistoryResolveService } from './admin-portal/saving-accounts/history/saving-account-transaction-history-resolve.service';
import { SavingAccountTypeService } from './admin-portal/saving-account-types/saving-account-type.service';
import { SavingAccountTypeResolveService } from './admin-portal/saving-account-types/saving-account-type-resolve.service';
import { EditSavingAccountTypeResolveService } from './admin-portal/saving-account-types/edit/saving-account-type-edit-resolve.service';
import { AdminSettingService } from './admin-portal/admin-settings/admin-setting.service';
import { AdminSettingResolveService } from './admin-portal/admin-settings/admin-setting-resolve.service';
import { EditAdminSettingResolveService } from './admin-portal/admin-settings/edit/admin-setting-edit-resolve.service';
import { TransactionHistoryService } from './admin-portal/transaction-histories/transaction-history.service';
import { TransactionHistoryResolveService } from './admin-portal/transaction-histories/transaction-history-resolve.service';
import { ReportService } from './admin-portal/reports/report.service';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        FlashComponent,
        PreLoaderComponent,
        LandingPageComponent,
        NavbarComponent,
        AdminPortalComponent,
        OverviewComponent,
        SavingAccountComponent,
        NewSavingAccountComponent,
        SavingAccountDetailComponent,
        EditSavingAccountComponent,
        DepositSavingAccountComponent,
        WithdrawSavingAccountComponent,
        SavingAccountHistoryComponent,
        SavingAccountTypeComponent,
        NewSavingAccountTypeComponent,
        EditSavingAccountTypeComponent,
        AdminSettingComponent,
        EditAdminSettingComponent,
        TransactionHistoryComponent,
        DailyIncomeReportComponent,
        MonthlySavingAccountsReportComponent
    ],
    providers: [
        AuthenticationService,
        AuthenticationGuard,
        AuthenticationResolveService,
        FlashService,
        PreLoaderService,
        DateFormatService,
        SavingAccountService,
        SavingAccountResolveService,
        SavingAccountDetailResolveService,
        EditSavingAccountResolveService,
        MinimumInitialResolveService,
        MinimumDepositResolveService,
        SavingAccountTransactionHistoryResolveService,
        SavingAccountTypeService,
        SavingAccountTypeResolveService,
        EditSavingAccountTypeResolveService,
        AdminSettingService,
        AdminSettingResolveService,
        EditAdminSettingResolveService,
        TransactionHistoryService,
        TransactionHistoryResolveService,
        ReportService
    ],
    bootstrap: [AppComponent, PreLoaderComponent]
})
export class AppModule { }
